/*
   @file validator.cc 
   @brief Validates a solution for an instance on the VRP with Time Windows and Carrier-Dependent Costs
   (VRPTWCD) for the formulation
   proposed in [Ceschia, Di Gaspero and Schaerf, 2011, Journal of Scheduling]

   This file contains all class and function definitions of the validator.
   It is compiled on Linux (or Windows+Cygwin) with GNU C++ with the command:
   g++ -o validator validator.cc
   It is used with the command (e.g., on instance case1-A):
     ./validator <input_file> <solution_file> [problem_formulation (default: vrptwcd)]
   For example on instance case1-A and solution my_sol.out
   ./validator case1-A.vrp my_sol.out
   ./validator Chu-H-01.vrp my_sol.out vrppc 

   We consider our problem formulation (vrptwcd) and the Vehicle Routing Problem 
   with Private fleet and Common carrier (vrppc) introduced in 

   M. Bolduc, J. Renaud, and F.F. Boctor, "A heuristic for the routing and carrier selection problem," European Journal of 
   Operational Research, vol. 183, 2007, pp. 926-932.

   Instances of the VPPC are available at http://www.mcbolduc.com/VRPPC/tests.htm.
 
   The input and output files are assumed to be correct.

   @author Sara Ceschia (sara.ceschia@uniud.it)
   Andrea Schaerf (schaerf@uniud.it), 
   Luca Di Gaspero (l.digaspero@uniud.it)
   @version 1.0
   @date 23 July 2010
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cctype> 
#include <utility>
#include <cassert>
#include <stdexcept>
//#include<math.h> 

const int BUF_SIZE = 200;

using namespace std;

/** Vehicle
    
A class that describes the input datas for a generic vehicle.

*/
class Vehicle
{
  friend ostream& operator<<(ostream& os , const Vehicle& v);
  friend istream& operator>>(istream& is, Vehicle& v);
  friend bool operator==(const Vehicle& v1, const Vehicle& v2);
 public:
  Vehicle(){}
  Vehicle(string id, unsigned capacity, unsigned fixed_cost, unsigned var_cost, string id_courier = "r0");
  Vehicle(const Vehicle& v);
  /**
     Gets the capacity in kg.
  */
  unsigned Capacity()const {return capacity;} 
  /**
     Gets the movimentation fixed cost in thousandths  of euro.
  */
  unsigned FixedCost()const {return fixed_cost;}
  /**
     Gets the variable cost for distance.
  */
  unsigned VarCost()const {return var_cost;}
  /**
     Gets the courier's id
  */
  string Courier() const {return id_courier;}
  /*
    Gets the progressive index of the vehicle, i.e. the position where the vehicle is stored in the vehicle_vect of VR_Input.
  */
  string Id()const {return id;}
  Vehicle& operator= (const Vehicle& v);
 protected:
  unsigned capacity, fixed_cost, var_cost;
  string id, id_courier;
};

/** Order
    
A class that describes the input datas for a generic order.

*/
class Order
{
  friend ostream& operator<<(ostream& os , const Order& o);
  friend istream& operator>>(istream& is, Order& o);
 public:
  Order(){}
  Order(string id_order, string id_client, unsigned qty, unsigned cost, bool obl = false, int start_dw = 1, int end_dw = 1);
  Order(const Order& o);
  /**
     Gets the output index of the client that makes the order
  */
  string IdClient() const {return client;} 
  /**
     Gets the last date of the the date window, in the form 1,2,...,n
  */
  int DateWindow() const {return date_window.second;}
  /**
     Gets the amout of the order in kg
  */
  unsigned Demand() const {return demand;}
  /**
     Gets the order's output index 
  */
  string Id() const {return id;}
  /**
     Gets the first date of the the date window, in the form 1,2,...,n-1
  */
  int InitialDateWindow() const {return date_window.first;}
  /** If true, this means that this order have to be delivered in the current plan
   */
  bool Obligation() const {return obligation;}
//   /**
//      Sets the date window
//      @param d1, the initial date 
//      @param d2, the final date
//   */
//   void SetDateWindow(int d1, int d2);
  Order& operator= (const Order& o);
  unsigned Cost () const {return cost;}
  void SetGroup(int g){group = g;}
  int GetGroup()const{return group;}
 protected:
  void SetCostByQty();
  string client, id;
  pair<int, int> date_window;
  unsigned demand;
  unsigned cost;
  bool obligation; 
  int group; 
};

/** OrderGroup
    
A class that describes the input datas for a group of orders that have the same:
- client
- date window
- obligation
*/
class OrderGroup
{
  friend ostream& operator<<(ostream& os , const OrderGroup& og);
 public:
  OrderGroup(){}
  /**
     Constructs a group order from the order o.
     @param o the order
  */
  OrderGroup(const Order& o); 
  OrderGroup(const OrderGroup& og);
  /**
     Adds a order to the group
     @param o, the output index of the order
     @param demand_order, the order's demand
  */
  void AddMember(string order, unsigned demand_order, unsigned cost_order); 
  void Clear(){members.clear();}
  /**
     Gets the output index of the client that makes the order
  */
   string IdClient() const {return client;} 
  /**
     Gets the last date of the the date window, in the form 1,2,...,n
  */
  int DateWindow() const {return date_window.second;}
  /**
     Gets the total demand of the all the orders that are in the group
  */
  unsigned Demand() const {return demand;}
  /**
     Gets true if the order o is compatible with the group
     @param o, the order
     @return true, if it's compatible 
  */
  bool GroupCompatibility(const Order& o)const; 
  /**
     Gets the first date of the the date window, in the form 1,2,...,n-1
  */
  int InitialDateWindow() const {return date_window.first;}
  /**
     If true, this means that this order have to be delivered in the current plan
  */
  bool Obligation() const {return obligation;}
  /**
     Gets the number of orders in the group
  */
  unsigned Size() const {return members.size();}
  /**
     Returns the order in the position i
     @param i, the position of the order in the member vector
     @return the output index of the order
  */
  string operator[](unsigned i) const {return members[i]; }
  OrderGroup& operator= (const OrderGroup& og);
  unsigned Cost () const {return cost;}
 protected: 
  string client; 
  pair<int, int> date_window;
  unsigned demand; 
  bool obligation; 
  vector<string> members;
  unsigned cost;
};

/** Client
    
A class that describes the input datas for a generic client.

*/
class Client
{
  friend ostream& operator<<(ostream& os , const Client& c);
  friend istream& operator>>(istream& is, Client& c);
  friend bool operator==(const Client& c1, const Client& c2);
 public:
  Client(){}
  Client(string id, int x_coord, int y_coord, string id_prov = "r0", int start_tw = 0, int end_tw = 0, unsigned service_time = 0);
  /**
     Gets the service/download time in seconds.
  */
  unsigned DownloadTime() const {return download_time;} 
  /**
     Gets the client's output index. 
  */
  string Id() const {return id;}
  /**
     Gets the id of the prov.
  */
  string IdProv()const{return id_prov;}
//   /**
//      Gets the index of the prov.
//      return -1, if the province isn't existing
//   */
//   int IndexProv(){return index_prov;}
//   /**
//      Set the province's index, i.e. the index where the province is stored in the prov_vect of VR_Input.
//   */
//   void SetIndexProv(int i);
//   /**
//      Set the time window in seconds.
//      @param t1, the time window low limit
//      @param t2, the time window up limit
//   */
//   void SetTimeWindow(int t1, int t2) { time_window.first = t1; time_window.second = t2;}
  /**
     Gets the time window in seconds.
  */
  int InitialTimeWindow()const {return time_window.first;}
  pair<int, int> TimeWindow() const {return time_window;}
  Client& operator= (const Client& c);
  double x_coord;
  double y_coord;
 protected:
  string id;
  unsigned download_time;
  //int index_prov;
  string id_prov;
  pair<int, int> time_window; 
};

class VR_Input;

/** Route
    
A class that describes the input datas for a generic route.

*/
class Route
{
  friend bool operator==(const Route& r1, const Route& r2);
 public:
  Route(unsigned ind, const VR_Input& in);
  /**
     If it's true, there's compatibility between the order and the route.
     @param o, the order
  */
  bool CompatibOrder(int order)const;
  /**
     Gets the day when the route planned.
  */
  unsigned Day()const;
 /**
     If it's true, there's compatibility between the order and the route because the province is covered by the courier.
     @param o, the order
  */
  bool DayVehicleCompatibility(int o) const;
  /**
     Gets the total demand of a route.
  */
  unsigned Demand()const;
  /**
     Deletes the order in the position.
     @param position, the position
  */
  void Erase(unsigned position){orders.erase(orders.begin() + position);}
  /**
     Gets the route's index.
  */
  unsigned Index()const {return index;}
  /**
     Inserts the order in the route in the position.
     @param order
     @param position
  */
  void Insert(int order, unsigned position){orders.insert(orders.begin()+position, order);}
  /**
     If it's true, this is the exclusion list.
  */
  bool IsExList()const {return ex_list;}
  /**
     Gets the route's lenght.
  */
  int Length() const;
  /**
     Gets the order after the depot.
  */
  int NextDepot() const {return orders[1];}
  /**
     Gets the order before the depot.
  */
  int PrevDepot() const {return orders[orders.size()-2];}
  /**
     Sets the exclusion list.
  */
  void SetExList(){ex_list = true;}
  /**
     Definies the order of the route.
  */
  void SetOrders(vector<int> ord){orders = ord;}
  /**
     Gets the order's number of the route.
  */
  unsigned Size()const {return orders.size();}
  /**
     Gets the vehicle's index.
  */
  unsigned Vehic()const;
  Route& operator= (const Route& r);
  int operator[](unsigned i) const{return orders[i];}
  int& operator[](unsigned i) {return orders[i];}
 protected:
  bool ex_list;
  const VR_Input& in;
  unsigned index;
  vector<int> orders;
};

/** 
    BillingCostComponent
    A Cost Component class to compute the cost dependent on the carrier.
*/
class BillingCostComponent
{
 public:
  BillingCostComponent(const VR_Input& i, const long int& w, string n);
  virtual long int ComputeCost(const Route& r, unsigned index_route) const = 0;
  long int Cost(const Route& r, unsigned index_route) const { return weight * ComputeCost(r, index_route);}
  string Name()const{return name;}
  virtual void PrintViolations(const Route& r, unsigned index_route, ostream& os) const = 0;
  void SetWeight(const unsigned w) { weight = w; }
  long int Weight() const { return weight; } 
  virtual ~BillingCostComponent() {}
 protected:
  const VR_Input& in;
  const string name; 
  long int weight;
};

/** 
    DistanceBillingCostComponent
    This class compute the cost dependent on the distance (distance*cost_coef).
*/
class DistanceBillingCostComponent : public BillingCostComponent
{
 public:
  DistanceBillingCostComponent(const VR_Input& in,  const long int& weight, string name = "Distance Billing Cost Component");
  void PrintViolations (const Route& r, unsigned index_route, ostream& os)const;
  long int ComputeCost(const Route& r, unsigned index_route) const;
};

/** 
    LoadFarestClientBillingCostComponent
    This class compute the cost dependent on the load and on the farest province (load*cost_coef).
*/
class LoadFarestClientBillingCostComponent : public BillingCostComponent
{
 public:
  LoadFarestClientBillingCostComponent(const VR_Input& in, const long int& weight, string name = "Load Billing Cost Component");
  /**Gets the max rate and the load of the route
     @param r, the route
     @return first: max rate
     @return second: load [kg]
  */
  pair<unsigned, unsigned> MaxRateLoad(const Route& r) const;
  long int  ComputeCost(const Route& r, unsigned index_route) const;
  void PrintViolations(const Route& r, unsigned index_route, ostream& os)const;

};

/**
   DistanceLoadBillingCostComponent
   This class compute the cost dependent on the load and on the distance.
*/
class DistanceLoadBillingCostComponent : public BillingCostComponent
{
 public:
  DistanceLoadBillingCostComponent(const VR_Input& in,  const long int& weight, string name = "Distance Load Billing Cost Component");
  /**Gets the max rate and the load of the route
     @param r, the route
     @return first: max rate
     @return second: load [kg]
  */
  pair<unsigned, unsigned> MaxRateLoad(const Route& r) const;
  bool IsFull(const Route& r, unsigned index_route)const;
  void PrintViolations (const Route& r, unsigned index_route, ostream& os)const;
  long int ComputeCost(const Route& r, unsigned index_route) const;
};

/** 
    LoadRangeBillingCostComponent
    This class compute the cost dependent on the load and on the distance.
*/
class LoadRangeBillingCostComponent : public BillingCostComponent
{
 public:
  LoadRangeBillingCostComponent(const VR_Input& in, const long int& weight, string name = "Load Range Billing Cost Component");
  /**
     @param cr, the route
     @param load, the load of the route [kg]
     @return the range
  */
  unsigned FindRange(const Route& r, unsigned load)const;
  /**Gets the max rate of the route
     @param r, the route
     @return  max rate
  */
  unsigned MaxRate(const Route& r, unsigned range) const;
  long int  ComputeCost(const Route& r, unsigned index_route)const;
  void PrintViolations(const Route& r, unsigned index_route, ostream& os) const;

};

/** 
    LoadClientDependetBillingCostComponent
    This class compute the cost dependent on the load and on the province for each client(load*cost_coef).
*/
class LoadClientDependentBillingCostComponent : public BillingCostComponent
{
 public:
  LoadClientDependentBillingCostComponent(const VR_Input& in, const long int& weight, string name = "Load ClientDependent Billing Cost Component");
  long int  ComputeCost(const Route& r, unsigned index_route) const;
  void PrintViolations(const Route& r, unsigned index_route, ostream& os)const;

};


/**
   Billing
   A class that describes the different ways to compute the billing.
*/
class Billing
{
  friend ostream& operator<<(ostream& os, const Billing&);
  friend bool operator==(const Billing& cr1, const Billing& cr2);
 public:
  //Billing (string id, const vector<bool> c);
  Billing (string id, string t);
  //----------------------------------------------
  //      BILLING COST COMPONENT
  //----------------------------------------------
  void AddCostComponent(BillingCostComponent* cc);
  void ClearCostComponents();
  unsigned CostComponents() const { return cost_component.size(); }
  long int CostFunction(const Route& r, unsigned ind_r) const;
  BillingCostComponent& GetCostComponent(unsigned i) const { return *(cost_component[i]); }
  void PrintCostFunction(const Route& r, unsigned ind_r, std::ostream& os) const; 
  // //----------------------------------------------
  // //      BILLING DELTA COST COMPONENT
  // //----------------------------------------------
  // void AddExtDeltaCostComponent(BillingDeltaCostComponent* cdc); 
  // void AddInsDeltaCostComponent(BillingDeltaCostComponent* cdc);
  // void AddSubDeltaCostComponent(BillingDeltaCostComponent* cdc);
  // void AddSwapDeltaCostComponent(BillingDeltaCostComponent* cdc);
  // BillingDeltaCostComponent& ExtDeltaCostComponent(unsigned int i){ return *ext_delta[i]; }
  // BillingDeltaCostComponent& InsDeltaCostComponent(unsigned int i){ return *ins_delta[i]; }
  // BillingDeltaCostComponent& SubDeltaCostComponent(unsigned int i){ return *sub_delta[i]; }
  // BillingDeltaCostComponent& SwapDeltaCostComponent(unsigned int i){ return *swap_delta[i]; }
  // unsigned ExtDeltaCostComponents() const { return ext_delta.size(); }
  // unsigned InsDeltaCostComponents() const { return ins_delta.size(); }
  // unsigned SubDeltaCostComponents() const { return sub_delta.size(); }
  // unsigned SwapDeltaCostComponents() const { return swap_delta.size(); }
  /**
     @param r, the route
     @param ind_r, route's index
     @param order, the order group that has to be extracted 
     @param position, the position of the order group to extract
     @return the delta cost
  */
  long int DeltaCostFunctionExtraction(const Route& r, unsigned ind_r,unsigned order, unsigned position) const;
  /**
     @param r, the route
     @param ind_r, route's index
     @param order, the order group that has to be inserted
     @param position, the position where the order group has to be inserted
     @return the delta cost
  */
  long int DeltaCostFunctionInsertion(const Route& r, unsigned ind_r, unsigned order, unsigned position) const;
  /**
     @param r, the route
     @param ind_r, route's index
     @param order, the order group to put in the route
     @param position where we have to put the order 
     @return the delta cost
  */
  long int DeltaCostFunctionSubstitution(const Route& r, unsigned ind_r, unsigned order, unsigned position) const;
  /**
     @param r, the route
     @param ind_r, route's index
     @param order1, the first order group
     @param order2, the second order group
     @return the delta cost
  */
  long int DeltaCostFunctionSwap(const Route& r, unsigned ind_r, unsigned order1, unsigned order2) const;
  //----------------------------------------------
  //        SELECTORS
  //----------------------------------------------
  /**
     Gets the load value [kg] to pass from a cost band to an other.
  */
  virtual unsigned CrossValue(unsigned i)const{return 0;}
  /**
     Gets the value (0<value<1) to pass from a tariff to an other.
  */
  virtual unsigned FullValue()const{return 0;}
  /**
     Gets the courier's id.
  */
//   unsigned Id() const {return id;}
//   /**
//      Gets the distance cost coefficient [euro/km]
//   */
  virtual unsigned KmRate() const {return 0;}
  /**
     Gets the load cost coefficient [euro/kg]
     @param prov, the province's index
  */
  virtual unsigned LoadCost(unsigned prov)const{return 0;}
  /**
     Gets the load cost coefficient [euro/kg]
     @param prov, the province's index
     @param i, the cost band
  */
  virtual unsigned LoadCost(unsigned prov, unsigned i)const{return 0;}
//   /**
//      If it's true, the province is covered by the courier.
//      @param index_prov, the province's index
//      @return true, if the province is covered by the courier
//   */
//   bool LocCompatibility(unsigned index_prov){return loc_compatibility[index_prov];}
  /*
    Gets the cost band's number.
  */
  virtual unsigned NumRange()const{return 0;}
  /**
     Gets the courier's id.
  */
  string Id() const{return id;}
  string GetType() const {return type;}
  virtual ~Billing(){}
 protected:
  //vector<bool> loc_compatibility; 
  vector<BillingCostComponent*> cost_component;
  // vector<BillingDeltaCostComponent*> ext_delta;
  // vector<BillingDeltaCostComponent*> ins_delta; 
  // vector<BillingDeltaCostComponent*>  sub_delta;
  // vector<BillingDeltaCostComponent*>  swap_delta;
  string  id;
  string type;
  virtual void printOn(ostream& os) const;  
};

/*------------------------------------------------------------------------------
        LOAD KM BILLING
	(the billing depends on the load and the farthest customer or on the distance)
------------------------------------------------------------------------------*/
class  LoadKmBilling: public Billing 
{
 public: 
  LoadKmBilling(string id, string t)
    : Billing(id, t){}
  unsigned LoadCost(unsigned prov)const {assert(prov<load_cost.size()); return load_cost[prov];}
  unsigned KmRate() const {return km_rate;}
  unsigned FullValue()const {return full_load_value;}
  void ReadInputData(istream& is, unsigned num_regions);
 protected:
  vector<unsigned> load_cost;
  unsigned full_load_value; 
  unsigned km_rate;  
  void printOn(ostream& os) const;
};

/*------------------------------------------------------------------------------
        KM BILLING
	(the billing depends on the distance)
------------------------------------------------------------------------------*/
class  KmBilling: public Billing 
{
  friend istream& operator>>(istream& is, KmBilling& c);
 public:
  KmBilling(string id, string t)
    : Billing(id, t){}
  unsigned KmRate() const {return km_rate;}
 protected:
  unsigned km_rate; 
  void printOn(ostream& os) const;
};

/*------------------------------------------------------------------------------
        VAR LOAD BILLING
	(the billing depends on the level of load and the farthest customer)
------------------------------------------------------------------------------*/
class VarLoadBilling : public Billing 
{
 public: 
  VarLoadBilling(string id, string t)
    : Billing(id, t){}
  unsigned LoadCost(unsigned prov, unsigned i)const {assert((prov<load_cost.size())&&(i<load_cost[prov].size()));return load_cost[prov][i];}
  unsigned CrossValue(unsigned i)const {return cross_value[i];}
  unsigned NumRange()const{return num_range;}
  void ReadInputData(istream& is, unsigned num_regions);
 protected:
  vector<vector<unsigned> > load_cost;
  vector<unsigned> cross_value; //valore soglia tale che per un carico inferiore o maggiore uguale si ha costi diversi  
  unsigned num_range;
  void printOn(ostream& os) const;
};


/*------------------------------------------------------------------------------
        LOAD BILLING
	(the billing depends on load and the farthest customer or on the load and 
	the region of each customer)
------------------------------------------------------------------------------*/
class LoadBilling : public Billing 
{
 public: 
  LoadBilling(string id, string t)
    : Billing(id, t){} 
  unsigned LoadCost(unsigned prov)const{assert(prov<load_cost.size()); return load_cost[prov];}
  void ReadInputData(istream& is, unsigned num_regions);
 protected:
  vector<unsigned> load_cost;
  void printOn(ostream& os) const;
};

/**
   Courier
   A class that describes the input datas for a generic carrier.
*/

class Courier
{
  friend ostream& operator<<(ostream& os, const Courier&);
  friend bool operator==(const Courier& cr1, const Courier& cr2);
public:
  Courier (string id, const Billing* b);
  /**
     If it's true, the province is covered by the courier.
     @param index_prov, the province's index
     @return true, if the province is covered by the courier
  */
  bool LocCompatibility(unsigned index_prov)const{assert(index_prov<loc_compatibility.size()); return loc_compatibility[index_prov];}
  /**
     Get a pointer to the billing type
   */
  const Billing* GetBilling()const {return bl;}
  void SetLocCompatibility (const vector<bool> c){loc_compatibility = c;}
private:
  string id;
  const Billing* bl;
  vector<bool> loc_compatibility; 
};

/** VR_Input
    
A class that describes the input datas for a generic route.

*/

class VR_Input    
{
  friend ostream& operator<<(ostream& os, const VR_Input& in);
 public:
  /**
     Constructs the input route.
     @param instance, the input file name's 
     @param vrppc, true if the problem formulation is VRPPC
  */
  VR_Input(string instance, bool vrppc);
  /**
     Gets the 'i' client object.
     @param i, the progressive client index
     return the client object
  */
  const Client& ClientVector(unsigned i)const;
 /**
     Gets the compatibility between an ordergroup and route.
     @param og, it's the ordergroup's index
     @param route, it's the route s' index
  */
  bool CompatibOrdergroupRoute(int og, unsigned route)const;
//   /**
//      Gets the compatibility between a province and a courier.
//      @param prov, it's the province's index
//      @param courier, it's the courier s' index
//   */
//   bool CompatibProvCourier(int prov, unsigned courier)const;
  /**
     Gets a pointer to the courier object in the position 'i' in the courier_vect.
     @param i, the index
     @return the pointer to the courier
  */
  const Courier& CourierVector(unsigned i) const {return courier_vect[i];}
  /**
     Gets true if the order 'o' is deliverable on the 'd' day of the planning horizon with vehicle'v'
     @param o, the progressive order index
     @param d, the day 
     @param v, the vehicle
     @return true or false
     @abort if the order or the day or the vehicle isn't existing
  */
  bool DayVehicleCompatibility(int o, unsigned d, unsigned v = 0) const;
  /**
     Gets the departure time (start time window of the depot) in seconds.
     @return the time. 
  */	 
  int  DepartureTime()const {return client_vect[0].InitialTimeWindow()*60; }  
  /**
     Gets the distance between client 'i' and client 'j'.
     @param i the client's index 
     @param j the client's index 
     @return the distance between client 'i' and client 'j' in km
     @abort if one client isn't existing
  */   
  int Distance(unsigned i, unsigned j) const; 
//   /**
//      Gets true if the client is present
//      @param i the output client index
//      @return true, if the client is present, false otherwise
//   */
//   bool FindClient(string id) const;
  /**
     Gets the index of the province in the province vector
     @param id of the province
     @return the index
     @return -1 if the prov isn't presence
  */
  int FindProv(string id) const;
  /**
     Gets the progressive client index used by the system
     @param i the output client index
     @return the progressive client index
  */
  int IndexClient(string id) const;
  /**
     Gets the progressive courier index used by the system
     @param i the courier id
     @return the progressive courier index
  */
  int IndexCourier(string id) const;
  /**
     Gets the progressive order index used by the system
     @param id the output order index
     @return the progressive order index
  */
  int IndexOrder(string id) const;
  /**
     Gets the customers' number of this day.
     @return the number of the customer
  */
  unsigned NumCustomer() const {return number_customer;}
  /**
     Gets the orders' number of this day.
     @return the number of the orders
  */
  unsigned NumOrder() const {return number_order;}
  /**
     Gets the group orders' number of this day.
     @return the number of the group orders
  */
  unsigned NumOrderGroup() const {return number_ordergroup;}  
  /**
     Gets the province's number.
     @return the number of the provinces
  */
  unsigned NumProv() const {return num_prov;}  		    	       
  /**
     Gets the vehicles' number.
     @return the number of the vehicle
  */	       
  unsigned NumVehicle() const {return vehicle_vect.size();} 
  /**
     Gets the index of the province of a ordergroup
     @param og, the index of the ordergroup
     @return the index of the province.
  */
  int OrderGroupProv(int og) const {assert(og>=0&&og<int(number_ordergroup));return ordergroup_prov[og];}
  /**
     Gets the 'i' order group object
     @param i, the progressive order index
     return the order group object
  */
  const OrderGroup& OrderGroupVector(unsigned i)const;  
  /**
     Gets the 'i' order object
     @param i, the progressive order index
     return the order object
  */
  const Order& OrderVector(unsigned i)const;
  /**
     Gets the days' number of the planning horizon.
     @return the day's number
  */
  unsigned PlanHorizon() const;
  /**
     Gets the days' of the planning horizon.
     @return a vector of the start data and the end data of the planning horizon
  */
   pair<int, int> PlanHorizonDates() const {return plan_horizon;}
//   /**
//      Gets the way back cost coefficient.
//      @return the coefficient
//   */
//   int ReturnCoeff() const {return return_coeff;}
  /*
     Print some statistical indexes computed on the instance data:
     A. filling ratio of the vehicles 
     B. mean ratio between tw of clients and of the depot
     C. mean ratio between dw of orders and the planning period
     D. density of the matrix orders-vehicles
     E. % of mandatory orders
     F. mean % of excluded orders
     G. mean % of space taken by a order in a vehicle
  */
  void Statistics(ostream& os = cout) const;
  /**
     Gets the period in second to go from  client 'i' to client 'j'.
     @param i the client's progressive index 
     @param j the client's progressive index 
     @return the period in seconds
     @abort if one client isn't existing
  */   
  unsigned TimeDistance (unsigned j, unsigned i) const; //sec
  /**
     Gets the 'i' vehicle object
     @param i, the progressive vehicle index
     return the vehicle object
  */
  const Vehicle& VehicleVector(unsigned i)const; 

  string Name() const { return name; }
  double EuclideanDistance (double x1, double y1, double x2, double y2);
 private:
  void ReadOurFile(string instance); 
  void ReadVRPPCFile(string instance); 
  vector< vector<bool> >day_comp;
//   int departure_time;
  vector<vector<int> > distance; //metres
  pair<int, int> plan_horizon;
//   int return_coeff;
  vector<vector<int> > time_distance; // seconds

  unsigned number_customer;
  unsigned number_order;
  unsigned number_ordergroup;
  unsigned num_prov; //number of regions
  unsigned num_courier; //number of carriers
  unsigned num_billing; //number of billings

  vector<Client> client_vect; 
  vector<Order> order_vect; 
  vector<OrderGroup> ordergroup_vect; 
  vector<Vehicle> vehicle_vect; 
  //vector<string> prov_vect;
  vector<Courier> courier_vect;

  vector<Billing*> billing_vect;

  map<string, int> find_courier;
  map<string, int> find_billing;
  //vector<vector<int> > find_client;
  map<string, int> find_client;
  map<string, int> find_prov;
  vector<vector<bool> > comp_ovg;//compatibility order (client->region)- route(vehicle/day)
  vector<unsigned> ordergroup_prov;
 
  string name;
};

/** VR_Output
    
A class that describes the output datas for a generic route.

*/

class VR_Output
{
  /**
     Reads an VR_Output object in the format:
     ---------------------------------------------
     Day 1 :
     # 0  v1(50000) 5: o133 o75 o86 o87 o88 [3526]
     ...
     Unscheduled 3: o43 o22 o4 [1501]
     ---------------------------------------------
     @param is, input stream
     @param opr, the VR_Output object
     @return is
  */
  friend istream& operator>>(istream& is, VR_Output& opr);
  /**
     Displays an VR_Output object in the format:
     ---------------------------------------------
     Day 1 :
     # 0  v1(50000) 5: o133 o75 o86 o87 o88 [3526]
     ...
     Unscheduled 3: o43 o22 o4 [1501]
     ---------------------------------------------
     @param os, output stream
     @param opr, the VR_Output object
     @return os
  */	 
  friend ostream& operator<<(ostream& os, const VR_Output& opr);  
 public:
  /**
     Constructs the output route for a day.
     @param r, the VR_Input object
     @param vrppc, the problem formulation
  */
  VR_Output(const VR_Input& in, string filename, bool f_vrppc); 
  virtual ~VR_Output() {}
  /**
     Sets this object VR_Output as a copy of an other VR_Output object
     @param opr object to copy
  */
  VR_Output& operator=(const VR_Output& opr);
  /**
     Gets the arrive time of the order that is in the route r  in the position p.
     @param r the route
     @param p the position in the route
     return the time 
  */
  int ArriveTime(unsigned r, unsigned p) const;
  /**
     Gets the 'i' client object.
     @param i, the progressive client index
     return the client object
  */
  const Client& ClientVector(unsigned i)const {return in.ClientVector(i);}   
  /**
     Gets true if the order 'o' is deliverable the 'd' day of the planning horizon with vehicle v.
     @param o, the order's index
     @param r, the route's index 
     @return true or false
     @abort if the order or the day or the vehicle isn't existing
  */
  bool DayVehicleCompatibility(int o, unsigned r) const {  return routes[r].DayVehicleCompatibility(o);}  
  /**
     Gets the distance between client 'i' and client 'j'.
     @param i the  client's index
     @param j the other client's index
     @return the distance between client 'i' and client 'j' in km
     @abort if one client ins't existing
  */ 
  int Distance(unsigned i, unsigned j)const { return in.Distance(i,j); } 
  /**
     Gets the group order in the route 'i' in the position 'j'.
     @param i the route's index
     @param j the position's index
     @return the group order's index
     @abort if the route or the position indexes are not existing
  */
  int GroupOrder(unsigned i, unsigned j) const;
  /**
     Gets the progressive client index used by the system.
     @param i the output client index
     @return the progressive client index
  */
  int IndexClient(string id) const {return in.IndexClient(id);}  
//   /**
//      Gets the progressive courier index used by the system.
//      @param i the di_courier index
//      @return the progressive courier index
//   */
//   int IndexCourier(unsigned i)const {return in.IndexCourier(i);}
//   /**
//      Gets the progressive order index used by the system.
//      @param i the output order index
//      @return the progressive order index
//   */
//   int IndexOrder(int i) const {return in.IndexOrder(i);}
  /**
     Gets the customers' number.
     @return the number of the customer
  */
  unsigned NumCustomer() const {return in.NumCustomer();}
  /**
     Gets the orders' number of this day.
     @return the number of the orders
  */
  unsigned NumOrder() const {return in.NumOrder();}
  /**
     Gets the group orders' number of this day.
     @return the number of the group orders
  */
  unsigned NumOrderGroup() const {return in.NumOrderGroup();}  
  /**
     Gets the route's number without the n-route of the out orders.
     @return the number of the routes
  */
  unsigned NumRoutes() const; 
  /**
     Gets the vehicles' number.
     @return the number of the vehicle
  */
  unsigned NumVehicle() const {return in.NumVehicle();} 
  /**
     Gets the 'i' order group object.
     @param i, the progressive order index
     return the order group object
  */
  const OrderGroup& OrderGroupVector(unsigned i)const {return in.OrderGroupVector(i);}   
  /**
     Gets the 'i' order object.
     @param i, the progressive order index
     return the order object
  */
  const Order& OrderVector(unsigned i)const {return in.OrderVector(i);}
  /**
     Gets the days' number of the planning horizon.
     @return the day's number
  */
  int PlanHorizon() const {return in.PlanHorizon();} 
  /**
     Gets the days' of the planning horizon.
     @return a vector of the start data and the end data of the planning horizon
  */
  pair<int, int> PlanHorizonDates() const {return in.PlanHorizonDates();} 
  /**
     Gets the position of the 'o' group order.
     @param o the group order's index 
     @return a vector where the first element is the route's index, the second is the position in the route
     @abort if group order 'o' is not existing
  */
  pair<int,int> Position(unsigned o) const;
  /**
     Get the minutes (later than 1 hour) for the return time to the depot  
 
   */
  long int GetReturnDepotLate() const {return return_depot_late;}
  void SetReturnDepotLate(long int rt) const {return_depot_late = rt;}
  /**
     Gets the route 'i', with vehicle RouteVehicle(i) in the day RouteDay(i).
     @param i the route's index
     @return the route 'i'  
     @abort if the route 'i' is not existing
  */
  const Route& Routes(unsigned i)const;
  /**
     Gets the period in second to go from  client 'i' to client 'j'.
     @param i the client's progressive index 
     @param j the client's progressive index 
     @return the period in seconds
     @abort if one client isn't existing
  */   
  unsigned TimeDistance (unsigned j, unsigned i) const {return in.TimeDistance(j, i);} 
  /**
     Gets the timetable of route 'i', with vehicle RouteVehicle(i) in the day RouteDay(i).
     @param i the route's index
     @return the 'i' row of the timetable 2d matrix  
     @abort if the route 'i' is not existing
  */
  vector <int> TimeTable(unsigned i) const;  
  /**
     Gets the 'i' vehicle object
     @param i, the progressive vehicle index
     return the vehicle object
  */
  const Vehicle& VehicleVector(unsigned i)const {return in.VehicleVector(i);}
  void Clear(){ routes.clear();  timetable.clear();}
  /**
     Updates the time table after a change in the StateRoute
  */
  void UpdateTimeTable();
 protected:
  virtual void printOn(ostream& os) const;  

  /**
     It's a constant reference to an VR_Input object.
  */
  const VR_Input& in;
  /**
     It's a  2d matrix where route[i][k]:
     - i, is the 'i' the index the summarizes the day and the vehicle
     - k is the position in the route 
     route[i][k] is the order in the day RouteDay(i) with the vehicle RouteVehicle(i) 
     the last route is the the route of out orders
  */
  vector<Route> routes;
  /**
     It's a matrix where an element 'timetable[i][k]' contains the time(in seconds) the vehicle RouteVehicle(i), in the day RouteVehicle(i), delives the 'route[i][k]'order 
  */
  vector <vector<int> > timetable; 
  bool vrppc; // the problem formulation

  mutable int return_depot_late;
};

class VR_Validator
{
public:
  VR_Validator(const VR_Input & i, const VR_Output & s, bool q) 
    : in(i), st(s), VRPPC(q), DATEWINDOW_COST(q?0:30), TIMEWINDOW_COST(q?0:10), OUTORDER_COST(q?1:250), VEHICLE_COST(1), DISTANCE_COST(q?1:0) {}
  void PrintCosts(ostream& os) const;
  void PrintTotalCost(ostream& os) const;
  void PrintViolations(ostream& os) const;
private:
  unsigned CostsOnExtraLoad() const;   //Hard	       
  unsigned CostsOnLateReturn() const;	//Hard   
  unsigned CostsOnDateWindows() const; //Soft	       
  unsigned CostsOnTimeWindows() const; //Soft
  unsigned CostsOnOutOrders() const; //Soft
  unsigned FixedCostsOnVehicles() const; //Soft
  unsigned VariableCostsOnVehicles() const; //Soft
  unsigned CostsOnDistances() const; //Soft

  void PrintViolationsOnExtraLoad(ostream& os) const;
  void PrintViolationsOnLateReturn(ostream& os) const;
  void PrintViolationsOnDateWindows(ostream& os) const;
  void PrintViolationsOnTimeWindows(ostream& os) const;
  void PrintViolationsOnOutOrders(ostream& os) const;
  void PrintVehicleFixedCosts(ostream& os) const;
  void PrintVehicleVariableCosts(ostream& os) const;
  void PrintDistanceCosts(ostream& os) const;

  const VR_Input& in;  
  const VR_Output& st;
  const bool VRPPC; 
  const unsigned DATEWINDOW_COST;
  const unsigned TIMEWINDOW_COST;
  const unsigned OUTORDER_COST;
  const unsigned VEHICLE_COST;
  const unsigned DISTANCE_COST;
};

//END OF HEADER

int main(int argc, char* argv[])
{
  bool vrppc = false;
  if (argc != 3 && argc != 4)
    {
      string error = "Usage:  " + string(argv[0]) + " <input_file> <solution_file> [problem_formulation (default: vrptwcd)]";
      throw logic_error(error); 	
    }
  else if (argc == 3 || strcmp(argv[3], "vrptwcd") == 0)
    vrppc = false;
  else if (strcmp(argv[3], "vrppc") == 0)    
    vrppc = true; //Vehicle Routing Problem with Private fleet and Common carriers (Bolduc et al. EJOR(2007))
  else
    throw logic_error("Supported problem fornulations: VRPTWCD and VRPPC"); 	

  VR_Input input(argv[1], vrppc);
  VR_Output output(input, argv[2], vrppc);
  VR_Validator validator(input, output, vrppc);

  validator.PrintViolations(cout);
  cout << endl;
  validator.PrintCosts(cout);
  cout << endl;
  cout << "Summary: ";
  validator.PrintTotalCost(cout);
  return 0;
}

//DEFINITIONS

bool operator==(const Vehicle& v1, const Vehicle& v2)
{
  return (v1.id == v2.id) && (v1.id_courier == v2.id_courier); 
}

ostream& operator<<(ostream& os, const Vehicle& v)
{

  os << v.id << " " << v.capacity << " " << v.fixed_cost  << " " 
     << v.var_cost  << " " << v.id_courier; 
  return os; 
}

istream& operator>>(istream& is, Vehicle& v)
{
  char buffer[BUF_SIZE]; 
  /**
     VEHICLE_CAPACITY_COST_SECTION [vehicle_id][capacity][cost][id_courier]
  */
  is >> v.id >> v.capacity >> v.fixed_cost >> v.id_courier;

  v.fixed_cost *= 1000; 
  v.var_cost = 1; 
  is.getline(buffer, BUF_SIZE);
  return is; 
}

Vehicle ::  Vehicle(string i, unsigned c, unsigned fc, unsigned vc, string ic )
{
  id = i;
  capacity = c;
  fixed_cost = fc;
  var_cost = vc;
  id_courier = ic;
}

Vehicle :: Vehicle (const Vehicle& v)
{
  capacity = v.capacity;
  fixed_cost = v.fixed_cost;
  var_cost = v.var_cost;
  id = v.id;
  id_courier = v.id_courier;
}

Vehicle& Vehicle::operator= (const Vehicle& v)
{
  capacity = v.capacity;
  fixed_cost = v.fixed_cost;
  var_cost = v.var_cost;
  id = v.id;
  id_courier = v.id_courier;
  return *this; 
}

ostream& operator<<(ostream& os, const Order& o)
{
  os << "Id: " << o.id << ", client: " << o.client 
     << ", qty:  " << o.demand  << ", cost:  " << o.cost 
     << ", DW: " << o.date_window.first << "-" << o.date_window.second << ", obl: "; 
  if (o.obligation)
    os << "T, ";
  else 
    os << "F, "; 
  return os; 
}

istream& operator>>(istream& is, Order& o)
{
  /**
     DEMAND_SECTION : [order_id][client_id][quantity][obl]
     ORDERS: [order_id][client_id][quantity][mandatory][ready_date][due_date]
  */

  char buffer[BUF_SIZE]; 
  unsigned n, first_day, last_day;

  is >> o.id >> o.client >> o.demand >> n >> first_day >> last_day;

  o.date_window.first = first_day;
  o.date_window.second=  last_day;

  is.getline(buffer, BUF_SIZE);

  if (n==1)
    o.obligation = true;
  else
    o.obligation = false;

   o.SetCostByQty();

   o.group = -1;

  return is;
}

void Order ::SetCostByQty()
{
  cost = demand;
}

Order :: Order(string id_order, string id_client, unsigned qty, unsigned c, bool obl, int start_dw, int end_dw)
{
  id = id_order;
  client = id_client; 
  demand = qty;
  cost = c; 
  obligation = obl; 
  date_window.first = start_dw;
  date_window.second = end_dw;
  group = -1;
}

Order :: Order(const Order& o)
{
  id = o.id;
  client = o.client; 
  demand = o.demand;
  cost = o.cost; 
  obligation = o.obligation; 
  date_window.first = o.date_window.first;
  date_window.second = o.date_window.second;
  group = o.group;
}

Order& Order :: operator= (const Order& o)
{
  id = o.id;
  client = o.client; 
  demand = o.demand; 
  cost = o.cost; 
  obligation = o.obligation;
  date_window.first = o.date_window.first;
  date_window.second = o.date_window.second;
  group = o.group;
  return *this; 
}

ostream& operator<<(ostream& os , const Client& c)
{
  os << "Id: "  << c.id  << ", prov: " << c.id_prov
     << ", serv_time: " << c.download_time 
     << ", TW: " << c.time_window.first << "-" << c.time_window.second
     << ", coordinates: " << c.x_coord << " " << c.y_coord;
    
  return os; 
}

istream& operator>>(istream& is, Client& c)
{
  /**
     TIME_WINDOW_SECTION [client_id][ready_time][due_date][service_duration[min]]
     CLIENTS [client_id][region_id][ready_time][due_date][service_duration[min]]:
  */

  char buffer[BUF_SIZE]; 
  
  is >> c.id >> c.id_prov  >> c.time_window.first >>  c.time_window.second  >> c.download_time;
  c.download_time = c.download_time * 60;
  is.getline(buffer, BUF_SIZE);

  c.x_coord = 0;
  c.y_coord = 0;
  
  return is;
}

Client :: Client(string i, int x, int y, string ip, int start_tw, int end_tw, unsigned service_time)
{
  id = i; 
  x_coord = x;
  y_coord = y;
  id_prov = ip;  
  time_window.first = start_tw;
  time_window.second = end_tw;
  download_time = service_time;
}


Client& Client :: operator= (const Client& c)
{
  id = c.id; 
  download_time = c.download_time;
  id_prov = c.id_prov;
  time_window.first = c.time_window.first;
  time_window.second = c.time_window.second;
  x_coord = c.x_coord;
  y_coord = c.y_coord;

  return *this;
}

ostream& operator<<(ostream& os , const OrderGroup& og)
{

  os << "client: " << og.client << ", DW: " 
     << og.date_window.first << "-" << og.date_window.second 
     << ", orders: "; 
  for (unsigned i=0; i<og.members.size(); i++)
    {
      os << og.members[i] << ", ";
    }
  os << "Tot_qty: " << og.demand <<  ", Tot_cost " << og.cost <<", obligation: ";

  if (og.obligation)
    os << "T" << endl; 
  else 
    os << "F" << endl;
  return os; 
}

OrderGroup :: OrderGroup(const Order& o)
{
  client = o.IdClient();
  date_window.first = o.InitialDateWindow();
  date_window.second = o.DateWindow();
  demand = o.Demand();
  cost = o.Cost();
  obligation = o.Obligation();
  members.push_back(o.Id());
}

OrderGroup :: OrderGroup(const OrderGroup& og)
{
  client = og.client;
  date_window = og.date_window;
  demand = og.demand;
  cost = og.Cost();
  obligation = og.obligation;
  members.resize(og.Size());

  for(unsigned i=0; i<og.Size(); i++)
    members[i] = og[i]; 
}



bool OrderGroup :: GroupCompatibility(const Order& o)const
{
  return ((client==o.IdClient())&&(date_window.first == o.InitialDateWindow())
	  &&(date_window.second == o.DateWindow())&&(obligation == o.Obligation()));
}

void OrderGroup :: AddMember(string order, unsigned demand_order,unsigned cost_order)
{
  members.push_back(order);
  demand += demand_order; 
  cost += cost_order;
}

OrderGroup& OrderGroup::operator= (const OrderGroup& og)
{
  client = og.client;
  date_window = og.date_window;
  demand = og.demand;
  obligation = og.obligation;
  cost = og.Cost();

  members.clear();
  members.resize(og.Size());
  for(unsigned i=0; i<og.Size(); i++)
    {
      members[i] = og.members[i]; 
    }
  return *this;
   
}


ostream& operator<<(ostream& os, const Courier& cr)
{
  os << "Id " << cr.id << endl;
  os << "Loc compatibility " << endl;

  for (unsigned k=0; k<cr.loc_compatibility.size(); k++)
    {
      os << "(" << k << "," << cr.loc_compatibility[k] << ") ";
    }

  os << endl;
  os << *cr.bl << endl;; 

  return os;
}

Route :: Route(unsigned ind, const VR_Input& i)
  :in(i)
{
  index = ind;
  ex_list = false;
  orders.resize(0);
}

bool operator==(const Route& r1, const Route& r2)
{
  return ((r1.orders == r2.orders) && (r1.ex_list == r2.ex_list)); 
}

Route& Route ::operator= (const Route& r)
{
  orders = r.orders;
  index = r.index;
  ex_list = r.ex_list;
  return *this;
}
unsigned Route :: Day()const 
{
  assert(!IsExList());
  return index/in.NumVehicle();
}

bool Route :: DayVehicleCompatibility(int o) const 
{
  return in.DayVehicleCompatibility(o, Day(), Vehic());
}

unsigned Route :: Vehic()const 
{
  assert(!IsExList());
  return index%in.NumVehicle();
}

unsigned Route :: Demand() const 
{
  unsigned demand = 0;
  OrderGroup og;

  for (unsigned k=1; k<orders.size()-1; k++)
    { 
      og = in.OrderGroupVector(orders[k]);
      demand += og.Demand();
    }
  return demand;
}

int Route :: Length() const
{
  unsigned client1, client2, dim = orders.size();
  int  length = 0;
  OrderGroup og1, og2;

  // The route cost is the distances' sum
  for (unsigned k=0; k< dim-1; k++)
    {
      if (dim == 2) //E' la route vuota 
	{
	  client1 = client2 = 0;
	}
      else if(orders[k]==-1)
	{
	  client1 = 0;
	  og2 = in.OrderGroupVector(orders[k+1]);
	  client2 = in.IndexClient(og2.IdClient()); 
	}
      else if (orders[k+1]==-1)
	{
	  og1 = in.OrderGroupVector(orders[k]);
	  client1 = in.IndexClient(og1.IdClient()); 
	  client2 = 0;
	}
      else
	{
	  og1 = in.OrderGroupVector(orders[k]);
	  og2 = in.OrderGroupVector(orders[k+1]);
	  client1 = in.IndexClient(og1.IdClient()); 
	  client2 = in.IndexClient(og2.IdClient()); 
	}

      length += in.Distance(client1, client2);
    }     
  return length;
}

bool Route ::CompatibOrder(int order)const
{
  assert(order<int(in.NumOrderGroup()));
  /*
     The fictitious order -1 is compatible with every route
  */
  if(order == -1)
    return true;
  if (!ex_list)
    {

      /*
	An order and a route could not be compatible for 2 reasons:
	1. the day is not included in the delivery dates of the order (so you use the function DayVehicleCompatibility(int o, unsigned d, unsigned v = 0) )
	2. the customer is in a region not covered by the carrier owner of the vehicle (so you use CompatibProvCourier(prov, cour))

	We consider hard just the second one.	
      */

      return in.CompatibOrdergroupRoute(order,index);
      /** If you consider hard both 1 and 2
	  return CompatibProvCourier(index_prov, index_courier) && DayVehicleCompatibility(order, RouteDay(index_route));
      */
    }
  else
    {
      /*
	For the list of unscheduled orders (e.g. route = NumRoutes()), an order could not be compatible if it is mandatory
	(order.Obligation())
      */
      OrderGroup og = in.OrderGroupVector(order);
      return (!og.Obligation());
    }

}

BillingCostComponent :: BillingCostComponent(const VR_Input& i, const long int& w, string n)
  : in(i), name(n), weight(w)
{}

DistanceBillingCostComponent :: DistanceBillingCostComponent(const VR_Input& in, const long int& weight, string name)
  : BillingCostComponent(in, weight, name)
{}

long int DistanceBillingCostComponent :: ComputeCost(const Route& r, unsigned index_route) const
{
  Vehicle v = in.VehicleVector(r.Vehic());

  return (long int)(r.Length() * in.CourierVector(in.IndexCourier(v.Courier())).GetBilling()->KmRate());
}

void DistanceBillingCostComponent::PrintViolations(const Route& r, unsigned index_route, ostream& os) const
{
  Vehicle v = in.VehicleVector(r.Vehic());
  // os << "Route: " << index_route << " " << r.Vehic() << ", " << name << endl;
  os << "Total distance: " << r.Length() << " km, Computed cost " << ComputeCost(r, index_route)/1000.0 << " euro." << endl;
}

LoadFarestClientBillingCostComponent :: LoadFarestClientBillingCostComponent(const VR_Input& in, const long int& weight, string name)
  : BillingCostComponent (in, weight, name)
{}

pair<unsigned, unsigned> LoadFarestClientBillingCostComponent :: MaxRateLoad(const Route& r) const
{
  /**
     ml.first = max_rate;
     ml.second = load; [kg]
  */

  Client cl;
  int index_prov;
  OrderGroup og;
  pair<unsigned, unsigned> ml(0,0);
  unsigned client;
  Vehicle v = in.VehicleVector(r.Vehic());
  const  Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();
 

  for(unsigned k=1; k< r.Size()-1; k++)
    {
      og = in.OrderGroupVector(r[k]);
      client = in.IndexClient(og.IdClient());
      cl = in.ClientVector(client);
      index_prov = in.FindProv(cl.IdProv());
      if(ml.first < cr->LoadCost(index_prov))
	ml.first = cr->LoadCost(index_prov);
      ml.second += og.Demand();
    }

  return ml;
}

long int LoadFarestClientBillingCostComponent :: ComputeCost(const Route& r, unsigned index_route)const
{
  pair<unsigned, unsigned> ml = MaxRateLoad(r);
  long int cost;
  unsigned  max_rate = ml.first, load = ml.second;
  
  cost = load * max_rate; 
  return cost; 
}

void LoadFarestClientBillingCostComponent::PrintViolations(const Route& r, unsigned index_route, ostream& os) const
{
  Client cl;  
  unsigned max_rate = 0;
  int index_prov = -1, prov;
  OrderGroup og;
  unsigned client, load = 0;
  string cod_cl = "-1";
  Vehicle v = in.VehicleVector(r.Vehic());
  const  Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();
 
  //  os << "Route: " << index_route << " " << r.Vehic() << ", " << name << endl;

  for(unsigned k=1; k< r.Size()-1; k++)
    {
      og = in.OrderGroupVector(r[k]);
      client = in.IndexClient(og.IdClient());
      cl = in.ClientVector(client);
      index_prov = in.FindProv(cl.IdProv());
      assert(index_prov>-1);
      if(max_rate < cr->LoadCost(index_prov))
	{
	  max_rate = cr->LoadCost(index_prov);
	  prov = index_prov;
	  cod_cl = og.IdClient();
	}
      load += og.Demand();
    }

  if(r.Size()>2)
    os << "Farest client: " << cod_cl << ", region: " << index_prov << ", max load rate: " 
     << max_rate/1000.0 << ". ";

  os << "Total load: " << load/100.0 << " q, Computed cost "  << ComputeCost(r, index_route)/1000.0 << " euro." << endl;
 
}

LoadRangeBillingCostComponent :: LoadRangeBillingCostComponent(const VR_Input& in, const long int& weight, string name)
  : BillingCostComponent (in, weight, name)
{}

unsigned LoadRangeBillingCostComponent :: FindRange(const Route& r, unsigned load)const
{
  unsigned range;
  Vehicle v(in.VehicleVector(r.Vehic()));
  const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();

  if (load > cr->CrossValue(cr->NumRange()-2))
    range = cr->NumRange()-1;
  else
    {
      range = 0;
      while ( (range<(cr->NumRange()-2)) && (load > cr->CrossValue(range)))
	{
	  range++;
	}
    }

  return range;
}

unsigned LoadRangeBillingCostComponent :: MaxRate(const Route& r, unsigned range) const
{
  Client cl;
  unsigned max_rate = 0;
  int index_prov;
  OrderGroup og;
  unsigned client;
  Vehicle v(in.VehicleVector(r.Vehic()));
  const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();
 
  for(unsigned k=1; k< r.Size()-1; k++)
    {
      og = in.OrderGroupVector(r[k]);
      client = in.IndexClient(og.IdClient());
      cl = in.ClientVector(client);
      index_prov = in.FindProv(cl.IdProv());
      assert(index_prov>-1);

      if(max_rate < cr->LoadCost(index_prov, range))
	max_rate = cr->LoadCost(index_prov, range);
    }
  
  return max_rate;
}

long int LoadRangeBillingCostComponent :: ComputeCost(const Route& r, unsigned index_route)const
{
  long int cost;
  unsigned max_rate, load, range;
  Vehicle v(in.VehicleVector(r.Vehic()));
  const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();
  
  load =  r.Demand();
  range = FindRange(r, load);
  max_rate =  MaxRate(r, range);


  if (range>0) // to be sure that the cost function is monotonous descreasing 
    {
      unsigned threshold_cost =  MaxRate(r, range-1)*cr->CrossValue(range-1),
	threshold_qty;

      threshold_qty = unsigned(floor((double)threshold_cost/max_rate));
      if (load<=threshold_qty)
	cost = threshold_cost;
      else
	cost = load * max_rate;	    
    }
  else
    cost = load * max_rate;

  return cost;
}

void LoadRangeBillingCostComponent::PrintViolations(const Route& r, unsigned index_route, ostream& os) const
{
  Client cl;
  unsigned max_rate = 0, load = 0;
  int index_prov= -1, prov;
  OrderGroup og;
  unsigned client,  range;
  string cod_cl = "-1";
  Vehicle v = in.VehicleVector(r.Vehic());
  const  Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();

  //  os << "Route: " << index_route << " " << r.Vehic() << ", " << name << endl;
  load = r.Demand();
  range = FindRange(r, load);

  for(unsigned k=1; k< r.Size()-1; k++)
    {
      og = in.OrderGroupVector(r[k]);
      client = in.IndexClient(og.IdClient());
      cl = in.ClientVector(client);
      index_prov = in.FindProv(cl.IdProv());
      assert(index_prov>-1);
      if(max_rate < cr->LoadCost(index_prov, range))
	{
	  max_rate = cr->LoadCost(index_prov, range);
	  prov = index_prov;
	  cod_cl = og.IdClient();
	}
    }
 if(r.Size()>2)
   os << "Farest client: " << cod_cl  << ", region: " << index_prov << ", max load rate: " << max_rate/1000.0 << ". ";
 
 os << "Total load: " << load/100.0 << " q, Computed cost "  << ComputeCost(r, index_route)/1000.0 << " euro." << endl;

}

DistanceLoadBillingCostComponent::DistanceLoadBillingCostComponent(const VR_Input& in,  const long int& weight, string name)
  :BillingCostComponent( in, weight, name )
{}

bool DistanceLoadBillingCostComponent :: IsFull(const Route& r, unsigned ind_route)const
{
  unsigned load = r.Demand();
  float full;
  OrderGroup og;
  Vehicle v(in.VehicleVector(r.Vehic()));
  const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();

  full = cr->FullValue();

  full = full/100;

  if (load<(full*v.Capacity()))
    return false;
  else
    return true;
  
}


pair<unsigned, unsigned> DistanceLoadBillingCostComponent :: MaxRateLoad(const Route& r) const
{  
  Client cl;
  int index_prov;
  OrderGroup og;
  pair<unsigned, unsigned> ml(0,0);
  unsigned client;
  Vehicle v(in.VehicleVector(r.Vehic()));
  const  Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();

  /**
     ml.first = max_rate;
     ml.second = load;[kg]
  */

  for(unsigned k=1; k< r.Size()-1; k++)
    {
      og = in.OrderGroupVector(r[k]);
      client = in.IndexClient(og.IdClient());
      cl = in.ClientVector(client);
      index_prov = in.FindProv(cl.IdProv());
      assert(index_prov>-1);
       if(ml.first < cr->LoadCost(index_prov))
	ml.first = cr->LoadCost(index_prov);
      ml.second += og.Demand();
    }
  
  return ml;
}

long int DistanceLoadBillingCostComponent :: ComputeCost(const Route& r, unsigned index_route)const
{
  long int cost = 0;
 
  if(IsFull(r,index_route))
    {
      long int  max_rate = 0;
      pair<unsigned, unsigned> ml = MaxRateLoad(r);
      unsigned load;

      max_rate = ml.first;
      load = ml.second;

      // Notice that the load is in kq not in q!
      cost = load * max_rate;
    }
  else
    {     
      Vehicle v(in.VehicleVector(r.Vehic()));
      const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();
      cost = r.Length()*cr->KmRate();
   
    } 
  return cost;
}

void DistanceLoadBillingCostComponent::PrintViolations(const Route& r, unsigned index_route, ostream& os) const
{
  Vehicle v(in.VehicleVector(r.Vehic()));
  const  Billing*  cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();

  if(IsFull(r, index_route))
    {
      // os << "Route: " << index_route << " " << r.Vehic() << ", " << name << endl;
      Client cl;
      unsigned max_rate = 0,load = 0, client;
      int index_prov=-1, prov;
      OrderGroup og;
      string cod_cl= "-1";
 
      for(unsigned k=1; k< r.Size()-1; k++)
	{
	  og = in.OrderGroupVector(r[k]);
	  client = in.IndexClient(og.IdClient());
	  cl = in.ClientVector(client);
	  index_prov = in.FindProv(cl.IdProv());
	  assert(index_prov>-1);
	  if(max_rate < cr->LoadCost(index_prov))
	    {
	      max_rate = cr->LoadCost(index_prov);
	      prov = index_prov;
	      cod_cl = og.IdClient();
	    }
	  load += og.Demand();
	}
      if(r.Size()>2)
	os << "Farest client: " << cod_cl  << ", region: " << index_prov << ", max load rate: " 
	   << max_rate/1000.0 << ". ";
  
      os << "Total load: " << load/100.0 << " q, "
	 << "Computed cost "  << ComputeCost(r, index_route)/1000.0 << " euro." << endl;
    }
  else
    {
      //     os << "Route: " << index_route << " " << r.Vehic() << ", " << name << endl;
      os << "Total distance: " << r.Length() << " km, Computed cost " << ComputeCost(r, index_route)/1000.0 << " euro." << endl;
    }
}


LoadClientDependentBillingCostComponent :: LoadClientDependentBillingCostComponent(const VR_Input& in, const long int& weight, string name)
  : BillingCostComponent (in, weight, name)
{}


long int LoadClientDependentBillingCostComponent :: ComputeCost(const Route& r, unsigned index_route)const
{
  unsigned rate; 
  long int cost = 0;
  int index_prov;

   Vehicle v(in.VehicleVector(r.Vehic()));
   const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();   

  for(unsigned k=1; k< r.Size()-1; k++)
    {
      index_prov = in.OrderGroupProv(r[k]);
      rate = cr->LoadCost(index_prov);
      cost += in.OrderGroupVector(r[k]).Demand()*rate;
    }

  return cost;
}

void LoadClientDependentBillingCostComponent ::PrintViolations(const Route& r, unsigned index_route, ostream& os) const
{
  // os << "Route: " << index_route << " " << r.Vehic() << ", " << name << endl;

  long int  cost = 0;
  int index_prov;
  unsigned qty, rate, load = 0; 

  Vehicle v(in.VehicleVector(r.Vehic()));
  const Billing* cr = in.CourierVector(in.IndexCourier(v.Courier())).GetBilling();   

  for(unsigned k=1; k< r.Size()-1; k++)
    {
      index_prov = in.OrderGroupProv(r[k]);
      rate = cr->LoadCost(index_prov);
      qty = in.OrderGroupVector(r[k]).Demand();
      load += qty;
      cost += qty*rate;
    }

  os << "Total load: " << load/1000.0 << " q, Computed cost "  << cost/1000.0 << " euro." << endl;
}

Courier :: Courier (string i, const Billing* b)
  :bl(b)
{
  id = i;
}

bool operator==(const Courier& cr1, const Courier& cr2)
{
  return cr1.id == cr2.id;
}


bool operator==(const Billing& cr1, const Billing& cr2)
{
  return cr1.id == cr2.id;
}

Billing :: Billing(string i, string t)  
{
  id = i;
  type = t;
}

ostream& operator<<(ostream& os, const Billing& cr)
{
  cr.printOn(os);
  return os;
}

void Billing :: printOn(ostream& os)const
{
  os << "Id:  " << Id()  << ", Type: "<< type <<", Cost components:" << endl;

  for (unsigned i=0; i< cost_component.size(); i++)
    {
      os << "  - " << cost_component[i]->Name() << endl;
    }
}

void Billing :: AddCostComponent(BillingCostComponent* cc)
{
  cost_component.push_back(cc);
}

void Billing :: ClearCostComponents()
{
  cost_component.clear();
}

long int Billing :: CostFunction(const Route& r, unsigned ind_r) const
{
  long int cost = 0;

  for (unsigned i = 0; i < cost_component.size(); i++)
    cost += cost_component[i]->Cost(r, ind_r);
   
  return cost; 
}

void Billing ::PrintCostFunction(const Route& r, unsigned ind_r, std::ostream& os) const
{
  for (unsigned i = 0; i < cost_component.size(); i++)
    cost_component[i]->PrintViolations(r, ind_r, os);
}

void LoadKmBilling :: printOn(ostream& os)const
{
  os << "Id:  " << Id()  << ", Type:  " << type <<", Cost components:" << endl;
  for (unsigned i=0; i< cost_component.size(); i++)
    {
      os << "- " << cost_component[i]->Name() << endl;
    }
  os << "Full load value " << full_load_value << endl
     << "Km rate "  << km_rate << endl;

  for (unsigned k=0; k<load_cost.size(); k++)
    {
      os << "(" << k << ";" << load_cost[k] << ") ";
    }
}

void LoadKmBilling :: ReadInputData(istream& is, unsigned num_regions)
{
  /**
     KIND : 2 [dist_cost_coeff][percentage_value] [number_of_load bands][load_cost_coeff] (già letto)
     0.88 0.9 1
     0.026 0.026 0.026 0.026 0.03 0.0285 0.0407 0.037 0.039 0 0.03 0.0325 0.0407 0.037 0.0285 0.0325 0.026 0.0285 0.03
  */

  char buffer[BUF_SIZE]; 
  double n;

  is  >> n;
  km_rate = unsigned(ceil(n*1000)); 

  is >> n;
  full_load_value = unsigned(n*100); 

  is.getline(buffer, BUF_SIZE);

  load_cost.resize(num_regions);

  for (unsigned i=0; i<load_cost.size(); i++)
    {
      is  >> n;
      load_cost[i] = unsigned(ceil(n*1000));
    } 

  is.getline(buffer, BUF_SIZE);
}

void KmBilling :: printOn(ostream& os)const
{
  os << "Id:  " << Id()  << ", Type:  " << type <<", Cost components:" << endl;
  for (unsigned i=0; i< cost_component.size(); i++)
    {
      os << "- " << cost_component[i]->Name() << endl;
    }
}

istream& operator>>(istream& is, KmBilling& c)
{
  char buffer[BUF_SIZE]; 
  /**
     KIND : 1 [dist_cost_coeff] (già letto)
     0.732 
  */

  double n;

  is  >> n;
  c.km_rate = unsigned(ceil(n*1000)); 

  is.getline(buffer, BUF_SIZE);
 
  return is;
}

void VarLoadBilling :: printOn(ostream& os)const
{
  os << "Id:  " << Id()  << ", Type:  " << type <<", Cost components:" << endl;
  for (unsigned i=0; i< cost_component.size(); i++)
    {
      os << "- " << cost_component[i]->Name() << endl;
    }

  os << "Load cost:" << endl;

  for (unsigned k=0; k<load_cost.size(); k++)
    {

      os << "(" << k << ":";
      for(unsigned j=0; j<load_cost[k].size(); j++)
	os << load_cost[k][j] << ";";
      os << ") ";
    }
  os << "Cross value: ";
    for(unsigned i=0; i< cross_value.size(); i++)
      os << cross_value[i] << " ";
 
}

void VarLoadBilling :: ReadInputData(istream& is, unsigned num_regions)
{   
  /**
     KIND : 3 [number of load bands][value_load_band]	
     2 8000
     0.031 0.026; 0.031 0.026; 0.031 0.026; 0.031 0.026; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0 0; 0.031 0.026; 0 0; 0.031 0.026; 0.031 0.026; 0 0;
  */

  char buffer[BUF_SIZE]; 
  string s;
  double n;

  is >> num_range;
  cross_value.resize(num_range-1); 
  
  for(unsigned i=0; i<cross_value.size(); i++)
    {
      is >>  cross_value[i]; //in kg
    }
  is.getline(buffer, BUF_SIZE); 

  load_cost.resize(num_regions);

  for (unsigned i=0; i<load_cost.size(); i++)
    {    
      load_cost[i].resize(num_range);
      is.getline(buffer, BUF_SIZE, ';');
      s = buffer;
      istringstream instr(s);
      for (unsigned k=0; k<load_cost[i].size(); k++)
	{
	  //instr  >> load_cost[i][k];
	  instr  >> n;
	  load_cost[i][k] = unsigned(ceil(n*1000));
	}

    } 
  is.getline(buffer, BUF_SIZE); 
}

void LoadBilling :: ReadInputData(istream& is, unsigned num_regions)
{
  /**
     KIND : 4 [load_cost_coeff](già letto)
     0.031 0.031 0.031 0.031 0.043 0.035 0.048 0.043 0.043 0.04 0.04 0.043 0.048 0.043 0.033 0.04 0.031 0.033 0.043
  */

 
  char buffer[BUF_SIZE];
  double n;

  load_cost.resize(num_regions);

  for (unsigned i=0; i<load_cost.size(); i++)
    {
      is  >> n;
      load_cost[i] = unsigned(ceil(n*1000));
    } 

   is.getline(buffer, BUF_SIZE); 
}


void LoadBilling :: printOn(ostream& os)const
{
  os << "Id:  " << Id()  << ", Type:  " << type <<", Cost components:" << endl;
  for (unsigned i=0; i< cost_component.size(); i++)
    {
      os << "- " << cost_component[i]->Name() << endl;
    }

  os << "Load cost:" << endl;

  for (unsigned k=0; k<load_cost.size(); k++)
    {
      os << "(" << k << ";" << load_cost[k] << ") ";
    }
}


ostream& operator<<(ostream& os, const VR_Input& in)
{
  unsigned i, k;
  
  os << "NAME: " << in.name << endl
     << "NUM_CLIENTS: " << in.number_customer << endl
     << "NUM_ORDERS: " << in.number_order << endl
     << "NUM_ORDERGROUPS: " << in.number_ordergroup << endl
     << "NUM_VEHICLES: " << in.vehicle_vect.size() << endl
     << "NUM_CARRIERS: " << in.num_courier << endl
     << "NUM_BILLINGS: " << in.num_billing << endl
     << "NUM_REGIONS: " << in.num_prov << endl;

  os << endl; 

  os << "REGIONS:" << endl;

  for(map<string, int>:: const_iterator iter = in.find_prov.begin(); iter != in.find_prov.end(); ++iter ) 
    os << (*iter).first << " ";
 
  os << endl;

  os << endl; 

  os << "CARRIERS: " << endl;
  for(i=0; i<in.courier_vect.size(); i++)
    os << (in.courier_vect[i]) << endl;

  os << endl; 

  os << "VEHICLES: " << endl;

  for (i=0; i<in.NumVehicle(); i++ )
      os << in.vehicle_vect[i] << endl;
  os << endl; 

  os << "CLIENTS:" << endl;

  for (i=0; i<in.number_customer; i++)
    os << in.client_vect[i] << endl;

  os << endl; 

  os << "ORDERS:" << endl;

  for (i=0; i<in.NumOrder(); i++)
    os << in.order_vect[i] << endl;

  os << endl; 

  os << "ORDERSGROUP:" << endl;
  for(i=0; i< in.number_ordergroup; i++)
    os << "Id: " << i << ", " << in.ordergroup_vect[i] ;
    
  os << endl; 

  os << "DAY VEHICLE COMPATIBILITY MATRIX" << endl;
  os << "     ";
   
  for (unsigned i=0; i<in.NumVehicle()*in.PlanHorizon(); i++)
    {
      unsigned data = in.plan_horizon.first;
      data = data +int(i/in.NumVehicle());
      os  << data <<  "v" << i%in.NumVehicle() << "  "; 
    }

  os << endl;

  for (unsigned i=0; i< in.day_comp.size(); i++)
    {
      os << setw(2) << i ;
      for (unsigned k=0; k<in.day_comp[i].size(); k++)
  	{
	  os << " " ;
	  if (in.day_comp[i][k])
	    os << setw(4) << "T";
	  else 
	    os << setw(4) << "F";
	}
      os << endl;
    }

  os << endl; 

  os << "DISTANCES[KM]:" << endl;
  os <<  "      ";

  for (i=0; i<in.number_customer; i++) 
    os << setw(6) << in.client_vect[i].Id() << "  ";  
 
  os << endl;

  for (i=0; i<in.distance.size(); i++)
    {
      os << setw(4) << in.client_vect[i].Id() << "  ";
      for(k=0; k<in.distance[i].size(); k++)
	os  << setw(6) << in.distance[i][k] << "  " ;
      os << endl;
    }

  os << endl; 

  os << "DISTANCES[Minutes]:" << endl;
  os <<  "      ";

  for (i=0; i<in.number_customer; i++) 
    os << setw(5) << in.client_vect[i].Id() << "  ";  
 
  os << endl;

  for (i=0; i<in.time_distance.size(); i++)
    {
      os << setw(4) << in.client_vect[i].Id() << "  ";
      for(k=0; k<in.time_distance[i].size(); k++)
	os  << setw(5) << in.time_distance[i][k] << "  " ;
      os << endl;
    }

  os << endl; 

  os << "CAPACITY AVAILABLE: ";
  unsigned cap = 0;
  for (unsigned i=0; i<in.vehicle_vect.size();i++)
    cap += in.vehicle_vect[i].Capacity();
  os << cap << " kg for a day" << endl;
  os << "NUMBER OF PLAN HORIZON DAYS: " << in.PlanHorizon() << endl;
  cap *= in.PlanHorizon();
  os << "TOTAL CAPACITY ALAIBLE: " << cap << endl;

  unsigned dem = 0;
  for (unsigned i=0; i<in.ordergroup_vect.size(); i++)
    dem += in.ordergroup_vect[i].Demand();

 os << "TOTAL DEMAND: " << dem << endl;

 in.Statistics(os);

 return os;
}

// There isn't any costructor without arguments
// This constructor takes the name of input file ad the problem formulation 

VR_Input::VR_Input(string instance, bool vrppc)
{
  if(vrppc)
    ReadVRPPCFile(instance);
  else
    ReadOurFile(instance); 
}

  void VR_Input:: ReadOurFile(string instance) 
{  
  // Operations for reading from input file 'instance'
  ifstream input_data;
  input_data.open(instance.c_str());  //converts from string to char array
	
  if(input_data.fail())
    {
      cerr << "Error opening " << instance << "\n";
      exit(EXIT_FAILURE);
    }

  /**
     NAME : pac1
     TYPE : HVRPFDTW ???
     COMMENT :
     NUM_LOCATIONS : 44
     NUM_ORDERS : 139
     NUM_VEHICLES : 7
     NUMBER_CARRIERS : 4
     NUMBER_BILLINGS : 4
     NUMBER_REGIONS : 19
     EDGE_WEIGHT_TYPE : EXPLICIT
     EDGE_WEIGHT_FORMAT : FULL_MATRIX
     PLAN_HORIZON: 1 - 3;
  */

  //--------------------

  unsigned number_vehicle = 0;
  char ch, buffer[BUF_SIZE];
  unsigned i, k, j;
  int  n, cr, rg, day, vehicle, index_prov, cl; 
  string s, id_depot, id_cl, id_loc1, id_loc2,id_cr, id_rg ,id_b, id_bt;
  int loc1, loc2, td;
  double d; 


  //Reads the first row that contains the clients' number and the trucks' number

  input_data >> s >> s ; // NAME: pac1
  name = s;

  getline(input_data, s);
  getline(input_data, s); // TYPE: HVRPFDTW ???
  getline(input_data, s); // COMMENT:

  input_data.getline(buffer, BUF_SIZE, ':'); // NUM_LOCATIONS : 44
  input_data >> number_customer;
  getline(input_data, s);

  input_data.getline(buffer, BUF_SIZE, ':'); // NUM_ORDERS : 139
  input_data >> number_order;
  getline(input_data, s);

  input_data.getline(buffer, BUF_SIZE, ':'); //  NUM_VEHICLES : 7
  input_data >> number_vehicle;
  getline(input_data, s);

  input_data.getline(buffer, BUF_SIZE, ':'); //  NUMBER_COURIERS : 4
  input_data >> num_courier;
  getline(input_data, s);

  input_data.getline(buffer, BUF_SIZE, ':'); //  NUMBER_BILLINGS : 4
  input_data >> num_billing;
  getline(input_data, s);

  input_data.getline(buffer, BUF_SIZE, ':');//   NUMBER_REGIONS : 19
  input_data >> num_prov;
  getline(input_data, s);

  getline(input_data, s); //  EDGE_WEIGHT_TYPE : EXPLICIT
  getline(input_data, s); // EDGE_WEIGHT_FORMAT : FULL_MATRIX

  input_data.getline(buffer, BUF_SIZE, ':'); // PLAN_HORIZON: 1 - 3;
  input_data >> plan_horizon.first >> ch >> plan_horizon.second;
  getline(input_data, s); 

  /**
     DATA_SECTION
     DEPOT_SECTION : 0
  */

  input_data >> s; // DATA_SECTION
  input_data >> s >> s;//DEPOT_SECTION [depot_id]:
  input_data >> id_depot; //c0
  input_data >> s >> s; //REGIONS [region_id]: 
  /*
    REGIONS [region_id]: 
    r7 r9 r13 r14 r15 r24 r26 r32 r21 r23 r37 r39 r40 r42 r45 r47 r49 r54 r55
  */

  for (i=0; i<num_prov; i++)
    {
      input_data >> s;
      find_prov[s] = i;
    //input_data >> prov_vect[i];
    }
    
  //getline(input_data, s);


  //--------------------------------------------------------------------------------------
  //                          BILLING SECTION
  //--------------------------------------------------------------------------------------

  //Cost components

  //FIRST CARRIER KmBilling
  DistanceBillingCostComponent* dcc = new DistanceBillingCostComponent(*this, 1);

  //SECOND CARRIER LoadKmBilling
  DistanceLoadBillingCostComponent* dlcc = new DistanceLoadBillingCostComponent(*this, 1);

  //THIRD CARRIER VarLoadBilling
  LoadRangeBillingCostComponent* lrcc = new LoadRangeBillingCostComponent(*this, 1);


  //FOURTH CORRIERE LoadBilling with FarestClient
  //Cost component
  LoadFarestClientBillingCostComponent* lcc = new LoadFarestClientBillingCostComponent(*this, 1);

  //FIFTH CORRIERE LoadBilling with ClientDependent
  LoadClientDependentBillingCostComponent* lcdc = new LoadClientDependentBillingCostComponent(*this, 1);

  billing_vect.resize(num_billing);
  input_data >> s; //BILLINGS_DATA:
  
  for (k=0; k<num_billing; k++)
    {
      input_data >> id_b >> id_bt; // b1 bt1 [dist_cost_coeff]:
      input_data.getline(buffer, BUF_SIZE);  

      if (id_bt == "bt1")
	{
	  //FIRST CARRIER KmBilling
	  KmBilling* kmc = new KmBilling(id_b, id_bt);
	  input_data >> *kmc;  	    
	  kmc->AddCostComponent(dcc);
	  billing_vect[k] = kmc; 
	}
      else if (id_bt == "bt2")
	{
	  //SECOND CARRIER LoadKmBilling
	  LoadKmBilling* lkmc = new LoadKmBilling(id_b, id_bt);
	  lkmc->ReadInputData(input_data, num_prov);
	  lkmc->AddCostComponent(dlcc);
	  billing_vect[k] = lkmc;
	}
      else if (id_bt == "bt3")
	{
	  //THIRD CARRIER VarLoadBilling

	  VarLoadBilling* vlc = new VarLoadBilling(id_b, id_bt);
	  vlc->ReadInputData(input_data, num_prov);
	  vlc->AddCostComponent(lrcc);
	  billing_vect[k] = vlc;
	}
      else if (id_bt == "bt4")
	{
	  //FOURT CORRIERE LoadBilling with FarestClient
	  LoadBilling* lfc = new LoadBilling(id_b, id_bt);
	  lfc->ReadInputData(input_data, num_prov);
	  lfc->AddCostComponent(lcc);
	  billing_vect[k]=lfc;
	}
      else if (id_bt == "bt5")
	{
	  //FIFTH CARRIER LoadBilling with ClientDependent (the bill depends on the load and on the region of each customer)
	  LoadBilling* ldc = new LoadBilling(id_b, id_bt);
	  ldc->ReadInputData(input_data, num_prov);
	  ldc->AddCostComponent(lcdc);
	  billing_vect[k]=ldc;
	}
      else
	{
	  cout << "Billing's type not implemented" <<endl;
	  assert(false);
	}

      find_billing[id_b] = k;
    }
  
 
  /*
    CARRIERS [courier_id][billing_id][num_incompatible_regions][list_incompatible_regions]:
  */
  
  input_data >> s; //CARRIERS [courier_id][billing_id][num_incompatible_regions][list_incompatible_regions]:
  getline(input_data, s);
 

  vector< vector<bool> >prov_comp(num_courier, vector<bool>(num_prov, true));

  for (i=0; i< num_courier; i++)
    {
      input_data >> id_cr >> id_b >> n;

      courier_vect.push_back(Courier(id_cr, billing_vect[find_billing[id_b]]));
      find_courier[id_cr] = i;
      for (k=0; k<unsigned(n); k++)
	{
	  input_data >> id_rg;
	  rg = FindProv(id_rg);
	  prov_comp[i][rg] = false;
	}
    }

  for (i=0; i<prov_comp.size(); i++)
    courier_vect[i].SetLocCompatibility(prov_comp[i]);
  /**
     VEHICLES [vehicle_id][capacity][cost][carrier_id]:     
  */
  input_data >> s; 
  getline(input_data, s);

  vehicle_vect.resize(number_vehicle);
  for (unsigned i=0; i< number_vehicle; i++)
    input_data >> vehicle_vect[i];

  /**
     CLIENTS [client_id][region_id][ready_time][due_date][service_duration[min]]:
  */
  input_data >> s; 
  getline(input_data, s);

  client_vect.resize(number_customer);

  for(i=0; i<number_customer; i++)
    {
      input_data >> client_vect[i];
      find_client[client_vect[i].Id()] = i;
    }

  /**
     ORDERS: [order_id][client_id][quantity][mandatory][ready_date][due_date]
  */
  input_data >> s; 
  getline(input_data, s);

  order_vect.resize(number_order);
  for(i=0; i<number_order; i++)
    input_data >> order_vect[i];

  /**
     EDGE_WEIGHT_SECTION [client1_id][client2_id][distance[km]][time_distance[sec]]
  */
  input_data >> s;
  getline(input_data, s);

  distance.resize(number_customer, vector<int>(number_customer,-1)); 
  time_distance.resize(number_customer, vector<int>(number_customer,-1)); 

  n = number_customer*number_customer; 

  for(i=0; i<unsigned(n); i++)
    {
      input_data >> id_loc1 >> id_loc2 >> d >> td;
      loc1 = IndexClient(id_loc1);
      loc2 = IndexClient(id_loc2);
      time_distance[loc1][loc2] = td;
      distance[loc1][loc2] = unsigned(d + 0.5);
    }

  //-------------------------------------------------------------------------------------
  //                         ORDERGROUP SECTION 
  //(we group together orders with the same customer, delivery dates and mandatory/optional)
  //-------------------------------------------------------------------------------------

  OrderGroup og(order_vect[0]);
  bool add = false; 
  unsigned demand_group, client_group, ind_prov, max_capacity, index_courier;

  //NB: for the order o0
  ordergroup_vect.push_back(og);
  order_vect[0].SetGroup(0);


  for (i=1; i<number_order; i++)
    {
      for (k=0; k< ordergroup_vect.size(); k++)
	{
	  if (ordergroup_vect[k].GroupCompatibility(order_vect[i]))
	    {
	      demand_group = ordergroup_vect[k].Demand();

	      /* This routine checks if the group just created is too large to be loaded in an unique vehicle */

	      client_group = IndexClient(ordergroup_vect[k].IdClient());	      
	      ind_prov = find_prov[client_vect[client_group].IdProv()];	 
	      max_capacity = 0;
	      for (j=0; j<vehicle_vect.size(); j++)
		{		
		  index_courier = IndexCourier(vehicle_vect[j].Courier());
		   
		  if (courier_vect[index_courier].LocCompatibility(ind_prov))
		    if(vehicle_vect[j].Capacity()>max_capacity)
		      max_capacity = vehicle_vect[j].Capacity();
		}

	      if (demand_group + order_vect[i].Demand()<=max_capacity)
		{
		  ordergroup_vect[k].AddMember(order_vect[i].Id(), order_vect[i].Demand(), order_vect[i].Cost());
		  add = true; 
		  order_vect[i].SetGroup(k);
		}
	    }
	}

      if(!add)
	{
	  OrderGroup og(order_vect[i]);
	  order_vect[i].SetGroup(ordergroup_vect.size());
	  ordergroup_vect.push_back(og);
	}
      add = false;
    }

  number_ordergroup = ordergroup_vect.size();
  n = number_vehicle*PlanHorizon();

  day_comp.resize(number_ordergroup, vector<bool>(n, false));

  //Matrix of compatibility between an order and a vehicle/day 
  comp_ovg.resize(number_ordergroup, vector<bool>(n, false));
  
  for (i = 0; i < number_ordergroup; i++ )
    {
      for (k = 0; k < day_comp[i].size(); k++ )
	{
	  day = k/number_vehicle;
	  vehicle = k%number_vehicle;
	  day = day + 1;
	  if ((day >= ordergroup_vect[i].InitialDateWindow()) && (day <= ordergroup_vect[i].DateWindow()))
	    day_comp[i][k] = true;
	  cr = find_courier[vehicle_vect[vehicle].Courier()];
	  cl = find_client[ordergroup_vect[i].IdClient()];
	  index_prov = find_prov[client_vect[cl].IdProv()];
	  if(courier_vect[cr].LocCompatibility(index_prov))
	    comp_ovg[i][k] = true;
	}
    }

  //Vector order-region
  ordergroup_prov.resize(number_ordergroup);
  
  for (i = 0; i < number_ordergroup; i++ )
    {
      cl = find_client[ordergroup_vect[i].IdClient()];
      index_prov = find_prov[client_vect[cl].IdProv()];
      ordergroup_prov[i] = index_prov;
    }
}

const Client& VR_Input :: ClientVector(unsigned i)const
{
  assert(i<number_customer);
 return client_vect[i]; 
}  

bool VR_Input :: CompatibOrdergroupRoute(int og, unsigned route)const
{
  assert((og<int(number_ordergroup))&&(route<vehicle_vect.size()*PlanHorizon()));

  if(og == -1) // fictitious depot order
    return true;

  return comp_ovg[og][route];
}

bool VR_Input :: DayVehicleCompatibility(int o, unsigned d, unsigned v) const
{
  if (o == -1)
    return true;
  //It is the list of unscheduled orders
  if ((d*NumVehicle()+v) == (PlanHorizon()*NumVehicle()))
    return true;
  if (o >= int(number_ordergroup) || (o < -1))
    cout << "The group order " << o << " isn't existing." << endl;
  if(d >= PlanHorizon())
    cout << "The day " << d << " is out of the planning horizon." << endl;
 
  if(v >= NumVehicle())
    cout << "The vehicle " << v << " isn't existing." << endl;
  assert ((o < int(number_ordergroup) && (o > -1)) && d < PlanHorizon() && v < NumVehicle());
  return day_comp[o][d*NumVehicle() + v];
}

int VR_Input :: Distance(unsigned i, unsigned j)const
{
  assert(i<number_customer && j<number_customer);
  return distance[i][j];
}

int VR_Input :: FindProv(string i) const
{
  int index = -1;
  
  if(find_prov.find(i) == find_prov.end()) 
    {
      cout << "You have inserted the identifier " << i <<" of a province non-existent" << endl;
      assert(false);
    }
  else
    index = find_prov.find(i)->second;
  
  return index;
}

int VR_Input :: IndexClient(string id) const
{
  int index = -1;
 
  if(find_client.find(id) == find_client.end()) 
    {
      cout << "You have inserted the identifier " << id <<" of a client non-existent" << endl;
      assert(false);
      index = -1;
    }
  else
    index = find_client.find(id)->second;

 return index;
}



int VR_Input :: IndexCourier(string id) const
{
  int index = -1;
 
  if(find_courier.find(id) == find_courier.end()) 
    {
      cout << "You have inserted the identifier " << id <<" of a courier non-existent" << endl;
      assert(false);
      index = -1;
    }
  else
    index = find_courier.find(id)->second;

 return index;
}

int VR_Input :: IndexOrder(string id) const
{
  if (id == "-1")
    return -1;

  for (unsigned k=0; k < number_order; k++)
    {
      if(order_vect[k].Id() == id)
	return  k; 
    }  
   
    cout << "Order identifier " << id << " non existent"  << endl;
    return -2;
}

unsigned VR_Input :: PlanHorizon()const
{
  int data2 =  plan_horizon.second, data1 = plan_horizon.first;
  int days = data2 - data1;
  
  if (days < 0)
    cout << "Negative planning horizon!" << endl;
  
  assert(days >=0);
  
  return unsigned(days + 1);
}

const Order& VR_Input :: OrderVector(unsigned i)const 
{
  assert(i<number_order);
  return order_vect[i];
}  

const OrderGroup& VR_Input :: OrderGroupVector(unsigned i)const 
{
  
  assert (i<number_ordergroup); 
  return ordergroup_vect[i];
}   

unsigned VR_Input :: TimeDistance (unsigned j, unsigned i) const 
{
  assert(i<number_customer && j<number_customer);
  return unsigned(time_distance[i][j]);
}

const Vehicle& VR_Input :: VehicleVector(unsigned i)const 
{
  assert(i<NumVehicle());
  return vehicle_vect[i];
} 

void  VR_Input :: Statistics(ostream& os) const
{
  /*
     Print some statistical indexes computed on the instance data:
     A. filling ratio of the vehicles 
     B. mean ratio between tw of orders and of the depot
     C. mean ratio between dw of orders and the planning period
     D. density of the matrix vehicles-orders
     E. % of mandatory orders
     F. mean % of excluded orders
     G. mean % of space taken by a order in a vehicle
  */

  os << "Statistics on instance data:" << endl;

  unsigned o, v, c, cap_o = 0, cap_v = 0, n;
  
  /* 
     A. filling ratio of the vehicles:= 
     fr = sum(cap_orders)/(sum(cap_vehicles)*gg)
  */

  float fr;

  for (o=0; o<ordergroup_vect.size(); o++)
    cap_o += ordergroup_vect[o].Demand();
 
  for (v=0; v<vehicle_vect.size(); v++)
    cap_v += vehicle_vect[v].Capacity();
  
  fr = cap_o/(float)(cap_v * PlanHorizon());

 
  os << "Filling ratio of the vehicles = "
     << setprecision(4) << fr*100 << " %" <<endl;

  /*
    B. mean ratio between tw of orders and of the depot:=
    m_tw = sum(tw_orders)/(tw_depot)*num_orders
  */

  float m_tw = 0;
  pair<int, int> tw;

  for (o=0; o<ordergroup_vect.size(); o++)
    {
      c = IndexClient(ordergroup_vect[o].IdClient());
      tw = client_vect[c].TimeWindow();
      m_tw += (tw.second - tw.first);
    }

  tw = client_vect[0].TimeWindow();
  m_tw = m_tw/(float)((tw.second - tw.first)*number_ordergroup);

  os << "Mean ratio between tw of orders and of the depot = "
     << setprecision(4) << m_tw*100 << " %" << endl;

  /*
    C. mean ratio between dw of orders and the planning period
     d_tw = sum(dw_orders)/(planning_period)*num_orders
  */

  float m_dw = 0;

  for (o=0; o<ordergroup_vect.size(); o++)
    m_dw += (ordergroup_vect[o].DateWindow() - ordergroup_vect[o].InitialDateWindow() + 1);
    
  m_dw = m_dw/(float)(PlanHorizon()*number_ordergroup);

  os << "Mean ratio between dw of orders and the planning period = "
     << setprecision(4) << m_dw*100 << " %" << endl;

   /*
     D. density of the matrix vehicles-orders

     order -> client -> region
     vehicle -> carrier
  */

  unsigned r, og;
  float d; 
  n = 0;

  for (og=0; og<number_ordergroup; og++)
    {
      for (r=0; r<comp_ovg[og].size(); r++)
	
	if (comp_ovg[og][r])
	  n++;
    }

  d = n/(float)(NumVehicle()*PlanHorizon()*number_ordergroup);
  os << "Density of the matrix vehicles-orders = "
     << setprecision(4) << d*100 << " %" << endl;

  /*
     E. % of mandatory orders :=
     mo = num_mand_orders / num_orders
  */
   
  float mo;

  n=0;

  for (o=0; o<ordergroup_vect.size(); o++)
    if (ordergroup_vect[o].Obligation())
      n++;
    
  mo = n/(float)number_ordergroup;

  os << "% of mandatory orders = " 
     << setprecision(4) << mo*100 << " %" << endl;

  /*
    F. if (C>1) => mean % of excluded orders :=
    mean_order_capacity = sum(cap_orders)/num_orders
     m_eo = (sum(cap_orders)-(sum(cap_vehicles)*gg))/(mean_order_capacity*num_orders)
  */

  if (m_dw > 1)
    {
      float meo;
  
      meo = ((cap_o - cap_v)*PlanHorizon())/(float)cap_o;
      os << "Mean % of excluded orders = "
	 << setprecision(4) << meo*100 << " %" << endl;
    }
  
  /*
    G. mean % of space taken by a order in a vehicle :=
    mean_vehicle_capacity = sum(cap_vehicles)/num_vehicles
    m_ov = mean_order_capacity/mean_vehicle_capacity

  */

  float mov, moc, mvc;

  moc = cap_o/(float)number_ordergroup;;
  mvc = cap_v/(float)NumVehicle();
  mov = moc/mvc;

  os << "Mean % of space taken by a order in a vehicle = "  
     << setprecision(4) << mov*100 << " %" << endl;
}

void VR_Input:: ReadVRPPCFile(string instance) 
{  
  // Operations for reading from input file 'instance'
  ifstream input_data;
  input_data.open(instance.c_str());  //converts from string to char array
	
  if(input_data.fail())
    {
      cerr << "Error opening " << instance << "\n";
      exit(EXIT_FAILURE);
    }

  string s,  vehicle_id, customer_id;
  unsigned number_vehicle, c, v, demand, capacity;  
  double x_coord, y_coord;
  double cost, fixed_cost, var_cost;
  bool int_rounding = false;

  input_data >> s >> s >> s ; //NAME : CE-H-01
  name = s;
  getline(input_data, s);// \n
  getline(input_data, s);//COMMENT : E051-05e
  input_data >> s >> s >> number_customer; //CUSTOMERS : 50
  input_data >> s >> s >> number_vehicle; //VEHICLES : 4

  if (name.substr(0,3) == "Chu" || name.substr(0,1) == "B")
    int_rounding = true;

  number_order = number_customer;

  number_customer++; //to include the depot

  getline(input_data, s);// \n
  getline(input_data, s); // NODE          COORD_SECTION (X, Y)        DEMANDS       EXTERN_COST

  for (c=0; c<number_customer; c++) //NB: number_customer+depot
    {
      input_data >> customer_id >> x_coord >> y_coord >> demand >> cost;
      cost = ceil(cost*1000000);

      client_vect.push_back(Client(customer_id, int(x_coord), int(y_coord)));
      find_client[customer_id] = c;

      if (demand > 0)
	{
	  //1 customer = 1 order
	  order_vect.push_back(Order(customer_id, customer_id, demand, int(cost)));
	  
	  order_vect[order_vect.size()-1].SetGroup(order_vect.size()-1);
	  //ordergroup
	  ordergroup_vect.push_back(Order(customer_id, customer_id, demand, int(cost)));
	}
    }

  getline(input_data, s);// \n
  getline(input_data, s); // TRUCK: CAPACITY FIXED COST VAR. COST

  for (v=0; v<number_vehicle; v++)
    {
      input_data >> vehicle_id >> capacity >> fixed_cost >> var_cost;
      fixed_cost = ceil(fixed_cost*1000000); //because the euclidean distance is multiplied by 1000
      var_cost = ceil(var_cost*1000);
      vehicle_vect.push_back(Vehicle(vehicle_id, capacity, int(fixed_cost), int(var_cost)));
    }

//Set PLAN_HORIZON: 1 - 1
  plan_horizon.first = 1;
  plan_horizon.second = 1;

  //Set a unique region -> TO REMOVE
  num_prov = 1;
  find_prov["r0"] = 0;

  //Fill distance matrix
  distance.resize(number_customer, vector<int>(number_customer,-1)); 

  for (unsigned i=0; i< distance.size(); i++)
    {
      for (unsigned j=0; j< distance[i].size(); j++)
	{
	  if(int_rounding)
	    distance[i][j] = (int)(EuclideanDistance (client_vect[i].x_coord, client_vect[i].y_coord, client_vect[j].x_coord, client_vect[j].y_coord))*1000;
	  else
	    distance[i][j] = (int)(round(EuclideanDistance (client_vect[i].x_coord, client_vect[i].y_coord, client_vect[j].x_coord, client_vect[j].y_coord)*1000));
	}
    }

  number_ordergroup = number_order;

  day_comp.resize(number_ordergroup, vector<bool>(number_vehicle, true));
  comp_ovg.resize(number_ordergroup, vector<bool>(number_vehicle, true));
  ordergroup_prov.resize(number_ordergroup, 0);
}

double VR_Input:: EuclideanDistance (double x1, double y1, double x2, double y2)
{
  double distance; 
  distance = sqrt(pow((x1-x2), 2) + pow((y1-y2), 2));
  return distance;
}


istream& operator>>(istream& is, VR_Output& opr)
{
  /** Output format:
      Day 1 :                                                                                                                                      
      # 0  v1(50000) 5: o133 o75 o86 o87 o88 [3526]                                                                                          # 1  v2(15000) 9: o73 o74 o91 o41 o127 o128 o29 o30 o71 [13369]                                                              
      # 2  v3(15000) 0: [0]
  */
  
  const int MAX_LENGHT = 200; 
  char ch;
  char buffer[MAX_LENGHT];
  int num_vehicle = opr.NumVehicle(), group;
  string remainder,  s, id_order; 
  vector<int> ordergroup_vect;
  unsigned n = opr.in.PlanHorizon()*num_vehicle + 1, orders_in_route, o;

  ch = is.peek();
  while(ch != 'D')
    {
     is.get(ch);// ' ' 
     ch = is.peek();
    }
 
  opr.Clear();

  for (unsigned i=0; i< n-1; i=i+num_vehicle)
    {
      getline(is, s);// Day 1 :

    
      for (int j=0; j<num_vehicle; j++)
	{
	  Route r(i+j,opr.in);
	  is.getline(buffer, MAX_LENGHT, ')');
	  is >> orders_in_route;
	  is.get(ch);
	  ordergroup_vect.push_back(-1);

	  for (o = 0; o<orders_in_route; o++)
	    {
	      is >>  id_order;

	      group = opr.in.OrderVector(opr.in.IndexOrder(id_order)).GetGroup();
	      assert(group>-1);
	      if (group != ordergroup_vect.back())
		ordergroup_vect.push_back(group);     
	    }

	  ordergroup_vect.push_back(-1);

	  r.SetOrders(ordergroup_vect);
	  opr.routes.push_back(r);
	  ordergroup_vect.clear();
	  getline(is, s); 
	}
    }

  getline(is, s); 
  ch = is.peek();
  while(ch == ' ')
    {
     is.get(ch);
     ch = is.peek();
    }

  is >> s; 
  is >> orders_in_route;
  is.get(ch);
  Route r(n,opr.in);
  r.SetExList();

  ordergroup_vect.push_back(-1);

  for (o = 0; o<orders_in_route; o++)
    {
      is >>  id_order;
         group = opr.in.OrderVector(opr.in.IndexOrder(id_order)).GetGroup();
      assert(group > -1);
      if (group != ordergroup_vect.back())
	ordergroup_vect.push_back(group);     
    }
 
  ordergroup_vect.push_back(-1);
  
  r.SetOrders(ordergroup_vect);
  opr.routes.push_back(r);
  ordergroup_vect.clear();
  getline(is, s); 
  
  if(!opr.vrppc)
    opr.UpdateTimeTable();
   
  return is;
}

ostream& operator<<(ostream& os, const VR_Output& opr)
{
  opr.printOn(os);
  return os;
}

void VR_Output :: printOn(ostream& os) const
{
  pair<int,int> plan = in.PlanHorizonDates();
  Vehicle v; 
  unsigned order_in_route = 0, o, i, k;

  int day = -1;

  for(i=0; i<NumRoutes(); i++)
    {
      order_in_route = 0;

      if(int(routes[i].Day())!= day)
	{
	  day = routes[i].Day();
	  os << "Day " << plan.first + day << " :" << endl;
	}

      v = VehicleVector(routes[i].Vehic());

      os << "  #" << setw(2) << i 
	 << "  "  << setw(2) << v.Id()
	 << "(" << setw(4) << v.Capacity() << ") ";

      for (o=1; o< routes[i].Size()-1; o++)
	order_in_route+= in.OrderGroupVector(routes[i][o]).Size();

      os << order_in_route << ": ";
      
      for (k=1; k< routes[i].Size()-1; k++)
	{
	  const OrderGroup& og = in.OrderGroupVector(routes[i][k]);

	  for (o=0; o<og.Size(); o++)
	    os << og[o] << " ";
	}

      os << "[" << routes[i].Demand() << "]" << endl;
    }

  os << endl;

  order_in_route = 0;

  for (k=1; k< routes[NumRoutes()].Size()-1; k++)
    order_in_route += in.OrderGroupVector(routes[NumRoutes()][k]).Size();

  os << "Unscheduled " << order_in_route << ": ";

  for (k=1; k< routes[NumRoutes()].Size()-1; k++)
    {
      const OrderGroup& og = in.OrderGroupVector(routes[NumRoutes()][k]);
      
      for (o=0; o<og.Size(); o++)
	os << og[o] << " ";
    }

  os << "[" << routes[NumRoutes()].Demand() << "]" << endl;

} 

VR_Output::VR_Output(const VR_Input& in, string file_name, bool f_vrppc)
  : in(in), vrppc(f_vrppc)
{
  unsigned n = in.NumVehicle()*in.PlanHorizon()+1;
  timetable.resize(n, vector<int>(0));

  for (unsigned i=0; i< n; i++)
    {
      Route r(i, in);
      routes.push_back(r);
    }
  routes[n-1].SetExList();

  ifstream is(file_name.c_str());
  string error;

  if (is.fail())
    throw runtime_error("Cannot open solution file!"); 	

  is >> *this;
  is.close();
}

VR_Output& VR_Output :: operator=(const VR_Output& opr)
{
  if (this != &opr)
    {
      routes = opr.routes;
      timetable = opr.timetable;
    }
  return *this; 
}

int VR_Output :: GroupOrder(unsigned i, unsigned j) const
{
  if ( i >= routes.size() )
      cout << "The route " << i << " isn't existing." << endl;
  assert( i < routes.size());    
    
  if ( j >= routes[i].Size() )
    cout << "The position " << j <<" is not existing." << endl;
    
  return routes[i][j];    
}
unsigned VR_Output :: NumRoutes() const 
{
  return routes.size()-1;
} 

pair<int,int> VR_Output :: Position(unsigned o)const  // index from 0
{
  pair<int,int> position(2,-1);
 
  if( o >= NumOrderGroup())
    cout << "The group order " << o << " isn't existing" << endl;

  assert( o<NumOrderGroup());

 
  for (unsigned i=0; i<routes.size(); i++)
    {
      for (unsigned j=0; j<routes[i].Size(); j++)
	{
	  if ( routes[i][j] == int(o) )
	    {
	      position.first = i; //the route 'i'
	      position.second = j; // the position of the order 'o' in the route 'i'
	    }
	}
    }
  
  return position; 
}


const Route& VR_Output ::Routes(unsigned i)const
{ 
  if ( i >= routes.size() )
    cout << "The route " << i  << " is not existing." << endl;

  assert( i<routes.size());

  return routes[i];
}

int VR_Output :: ArriveTime(unsigned r, unsigned p)const
{
  if (r > NumRoutes())
    cout << "The route " << r << " isn't existing" << endl; 
  if (p >= timetable[r].size())
    cout << "The position " << p << " in the route " << r << " isn't existing" << endl;
  assert((r<=NumRoutes())&&(p<timetable[r].size()));
  return timetable[r][p];
}

vector <int> VR_Output :: TimeTable(unsigned i) const
{

  if ( timetable.size()-1 < i )
    cout << "The timetable " << i << " is not existing." << endl;

  assert( timetable.size() > i);

  return timetable[i];    
}

void VR_Output :: UpdateTimeTable()
{
  Client cl1, cl2;
  int  order1, order2, arrive_time = in.DepartureTime(), stop_time = in.DepartureTime();
  OrderGroup og1, og2;
  pair<int, int> time_window;
  unsigned  client1, client2, period; // period in sec

  timetable.resize(routes.size());

  for (unsigned i=0; i<routes.size(); i++)
    {
      timetable[i].resize(routes[i].Size());
      arrive_time = in.DepartureTime();
      stop_time = in.DepartureTime();
      timetable[i][0] = arrive_time;

      for(unsigned k=1; k<routes[i].Size(); k++)
	{
	  order1 = routes[i][k-1];
	  order2 = routes[i][k];
	  if (order1==-1 && order2==-1)
	    {
	      client1 = client2 = 0;
	    }
	  else
	    {
	      if(order1==-1)
		{
		  client1 = 0; 
		  og2 = OrderGroupVector(order2);		 
		  client2 = in.IndexClient(og2.IdClient());
		}
	      else if(order2==-1)
		{
		  client2 = 0;
		  og1 = OrderGroupVector(order1);
		  client1 = in.IndexClient(og1.IdClient());
		}
	      else
		{
		  og1 = OrderGroupVector(order1);
		  og2 = OrderGroupVector(order2);

		  client1 = in.IndexClient(og1.IdClient()); 
		  client2 = in.IndexClient(og2.IdClient());
		}
	    }
	  cl1 = ClientVector(client1);
	  cl2 = ClientVector(client2);	   

	  if (client1 != client2)
	    {
	      period = TimeDistance(client1, client2);

	      arrive_time = arrive_time + cl1.DownloadTime() + period ;

	      time_window =  cl2.TimeWindow();
	      time_window.first = time_window.first*60;  //time window in min e timetable in sec
	      time_window.second = time_window.second*60;

	      /**
		 Correction to respect the drivers' break time
		 (45 min (=2700 sec) after 4.5 h = 16200 sec)
	      */
 
	      if ((arrive_time - stop_time) > 16200)
		{
		  arrive_time = arrive_time + 2700;
 

		  if (arrive_time < time_window.first)
		    {
		      arrive_time = time_window.first;
		    }

		  stop_time = arrive_time;
		}

	      /**
		 Correction for advanced arrivals:
		 - an advanced arrivals is postponed until the time window's lower bound
	      */

	      else if (arrive_time < time_window.first)
		{
		  if (time_window.first - arrive_time >= 2700)
		    stop_time = time_window.first;
		  arrive_time = time_window.first;
		}
	    }

	  timetable[i][k] =  arrive_time;
	}
	
    }
}

void VR_Validator :: PrintCosts(ostream& os) const
{
  if (VRPPC)
    {
      os << "Violations of Extra Load (hard) : " <<  CostsOnExtraLoad() << endl;
      os << "Cost of Out Orders (soft) : " <<  CostsOnOutOrders() * OUTORDER_COST << endl;
      os << "Cost of Distances (soft) : " <<  CostsOnDistances() * DISTANCE_COST << endl;
      os << "Fixed Cost of Vehicles (soft) : " <<  FixedCostsOnVehicles() * VEHICLE_COST << endl;
    }
  else
    {
      os << "Violations of Extra Load (hard) : " <<  CostsOnExtraLoad() << endl;
      os << "Cost of wrong Delivery Dates (soft) : " <<  CostsOnDateWindows() * DATEWINDOW_COST << endl;
      os << "Cost of Late Arrivals (soft) : " <<  CostsOnTimeWindows() * TIMEWINDOW_COST << endl;
      os << "Violations of Late Return to depot (hard) : " <<  CostsOnLateReturn() << endl;
      os << "Cost of Out Orders (soft) : " <<  CostsOnOutOrders() * OUTORDER_COST << endl;
      os << "Fixed Cost of Vehicles (soft) : " <<  FixedCostsOnVehicles() * VEHICLE_COST << endl;
      os << "Variable Cost of Vehicles (soft) : " <<  VariableCostsOnVehicles() * VEHICLE_COST << endl;
    }
}

void VR_Validator :: PrintTotalCost(ostream& os) const
{
  unsigned violations, total_cost = 0;

  if (VRPPC)
    {
      total_cost =  CostsOnDistances() * DISTANCE_COST + CostsOnOutOrders() * OUTORDER_COST + FixedCostsOnVehicles() * VEHICLE_COST;
      violations = CostsOnExtraLoad();
    }
  else
    {
      total_cost =  CostsOnDateWindows() * DATEWINDOW_COST + CostsOnTimeWindows() * TIMEWINDOW_COST + 
	CostsOnOutOrders() * OUTORDER_COST + FixedCostsOnVehicles() * VEHICLE_COST + VariableCostsOnVehicles() * VEHICLE_COST;
      violations = CostsOnExtraLoad() + CostsOnLateReturn();
    }
 
  if (violations > 0)
    os << "Violations = " << violations << ", ";
  os <<  "Total Cost = " << total_cost  << endl;
}

void VR_Validator :: PrintViolations(ostream& os) const
{
  if (VRPPC)
    {
      PrintViolationsOnExtraLoad(os);
      PrintViolationsOnOutOrders(os);
      PrintVehicleFixedCosts(os);
      PrintDistanceCosts(os);
    }
  else
    {
      PrintViolationsOnExtraLoad(os);
      PrintViolationsOnDateWindows(os);
      PrintViolationsOnTimeWindows(os);
      PrintViolationsOnLateReturn(os);
      PrintViolationsOnOutOrders(os);
      PrintVehicleFixedCosts(os);
      PrintVehicleVariableCosts(os);
    }
}

unsigned VR_Validator :: CostsOnExtraLoad() const  //Hard
{
  unsigned route_capacity = 0;
  long int extra_load = 0;
  int dif;
  Vehicle v;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      route_capacity = st.Routes(i).Demand();
      v = st.VehicleVector(st.Routes(i).Vehic());
      dif = route_capacity - v.Capacity();
     if (dif > 0)
       extra_load += dif;
    }

  return extra_load; 
}
	       
unsigned VR_Validator :: CostsOnLateReturn() const	//Hard       
{
  //NB: You MUST call before CostsOnTimeWindows(), because these cost function are coupled
  return st.GetReturnDepotLate();
}

unsigned VR_Validator :: CostsOnDateWindows() const //Soft
{
  long int qty = 0; 
  OrderGroup og;
  
  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      if(r.Size()>2)
	{
	  for (unsigned k=1; k< r.Size()-1; k++)
	    {
 	      if (!st.DayVehicleCompatibility(r[k], i))
 		{
		  og = in.OrderGroupVector(r[k]);
		  qty+=og.Demand();
		}
	    }  
	} 
    }     

  return qty;
}
	       
unsigned VR_Validator :: CostsOnTimeWindows() const //Soft
{
  bool over_time = false; //introdotto per evitare che i viaggi oltrepassino la giornata (ovvero durino 24 ore)
  Client cl;

  int late = 0, num_ord_depot_late = 0;

  int  a=0, client, day = -1, final_day = 86399, order; 
  OrderGroup og;
  pair<int,int> time_window, time_window_depot = in.ClientVector(0).TimeWindow();
  vector<int> timetable(0); 
  unsigned size = 1;

  //time window in minutes e timetable in seconds
  time_window_depot.second = time_window_depot.second*60;
 
  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      a=0;
      over_time= false;
      day = -1;
      timetable = st.TimeTable(i);

      for (unsigned k=0; k<r.Size(); k++)
	{	 
	  order = r[k];
	  if (order == -1)
	    {
	      client = 0;
	      size = 1;
	    }
	  else
	    {
	      og = st.OrderGroupVector(order); 
	      client = in.IndexClient(og.IdClient());
	      size = og.Size();
	    }
	  cl = in.ClientVector(client);
	  time_window = cl.TimeWindow(); 
	  time_window.first = time_window.first*60;  //time window in min e timetable in sec
	  time_window.second = time_window.second*60;
	  if (k>=1)
	    {
	      if(timetable[k]<timetable[k-1])
		{
		  over_time = true;
		  day++;
		}
	    }	    

	  if ( (timetable[k] > time_window.second) || over_time)
	    {
	      if (over_time)
		{
		  a = final_day - time_window.second;
		  a = a + timetable[k] + day*86400; 
		  late+= a*size;
		  if (k == r.Size()-1)
		    num_ord_depot_late+= int(r.Size())-2;
		}
	      else
		{
		  late+= (timetable[k]-time_window.second)*size;
		  if (k == r.Size()-1 && (timetable[k]-time_window.second) > 3600) // later than 1 hour
		    {
		      num_ord_depot_late+= int(r.Size())-2;
		    }
		}
	
	    }
	}
 
    }

  st.SetReturnDepotLate(num_ord_depot_late);
  return late; //in sec
}

unsigned VR_Validator :: CostsOnOutOrders() const //Soft
{
  Route r = st.Routes(st.NumRoutes());
  OrderGroup og; 
  long int cost = 0;
  for (unsigned i=1; i< r.Size()-1; i++) 
    {
      og = st.OrderGroupVector(r[i]);
      cost += og.Cost();
    }
  return cost;
}

unsigned VR_Validator :: FixedCostsOnVehicles() const //Soft
{
  long int cost = 0;
  Vehicle v;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      v = st.VehicleVector(r.Vehic());
      if (r.Size() > 2 )
	cost += v.FixedCost();	
    }

  return cost;
}

unsigned VR_Validator :: VariableCostsOnVehicles() const //Soft
{
  unsigned ind_courier;
  long int cost = 0;
  Vehicle v;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      v = st.VehicleVector(r.Vehic());
      ind_courier = in.IndexCourier(v.Courier());
      cost+= in.CourierVector(ind_courier).GetBilling()->CostFunction(r, i);   
    }

  return cost;
}

// unsigned VR_Validator :: CostsOnVehicles() const //Soft
// {
//   unsigned ind_courier;
//   long int cost = 0;
//   Vehicle v;

//   for(unsigned i=0; i<st.NumRoutes(); i++)
//     {
//       Route r = st.Routes(i);
//       v = st.VehicleVector(r.Vehic());
//       ind_courier = in.IndexCourier(v.Courier());
      
//       cost+= in.CourierVector(ind_courier).GetBilling()->CostFunction(r, i);   
//       if (r.Size() > 2 )
// 	cost += v.FixedCost();	
//     }

//   return cost;
// }

unsigned VR_Validator :: CostsOnDistances() const //Soft
{
  long int route_length = 0;

  for(unsigned i=0; i<st.NumRoutes(); i++)
      route_length += st.Routes(i).Length()*in.VehicleVector(st.Routes(i).Vehic()).VarCost();

  return route_length; 
}

void VR_Validator :: PrintViolationsOnExtraLoad(ostream& os) const
{
  int extra_load = 0, dif;
  Vehicle v;

  os << "Violations on Extra Load" << endl;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      v = st.VehicleVector(r.Vehic());
      dif = r.Demand() - v.Capacity();

      if (dif > 0)
	{

	  os << "Route: " << setw(2) << i 
	     << ", capacity: " << setw(6) << v.Capacity() << " kg"
	     << ", load: " << setw(6) << r.Demand() << " kg"
	       	 << ", extra load: " << setw(6) << dif << " kg" << endl;
	  extra_load += dif;
	}
    }
  os << "Total Extra Load: " << extra_load << " kg" << endl << endl;
}

void VR_Validator :: PrintViolationsOnLateReturn(ostream& os) const
{
  //NB: You MUST call before CostsOnTimeWindows(), because these cost function are coupled
  os << "Violations on shutdown time" << endl;
  os << "Orders in a routes coming back after the shutdown time: " << st.GetReturnDepotLate() << endl << endl;
}

void VR_Validator :: PrintViolationsOnDateWindows(ostream& os) const
{
  int day = -1, count = 0, qty = 0, total_qty = 0; 
  OrderGroup og;
  bool print = true;

  os << "Violations on Delivery Dates" << endl;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      qty = 0;

      if(r.Size()>2)
	{
	  for (unsigned k=1; k< r.Size()-1; k++)
	    {
 	      if (!st.DayVehicleCompatibility(r[k], i))
 		{
		  if (print)
		    {
		      if(day != int(r.Day()))
			{
			  os << "Day " << r.Day() << endl;
			  day = r.Day();
			}
		      os << "Route # " << setw(2) << i << ", Orders in wrong day: " ;
		      print = false;
		    }

		  og = st.OrderGroupVector(r[k]);
		  //		  os << r[k] << "(";
		  for(unsigned  j=0; j<og.Size(); j++)
		    {
		      os  << " " << og[j] ;
		    }
		  // os << ") ";
		  count += og.Size();
		  qty += og.Demand();
		  total_qty += og.Demand();
		}
	    }  
	}  

      if(!print)
	os << ", Cost  " << setw(5) << DATEWINDOW_COST * qty << endl;      
      print = true;      
    }

  os << "Total number of orders delivered in wrong days: " << count  << endl
    //    << "Total Cost: " << DATEWINDOW_COST * total_qty << endl << endl;
    << "Total Cost: " <<  CostsOnTimeWindows() << endl << endl;
}

void VR_Validator :: PrintViolationsOnTimeWindows(ostream& os) const
{
  bool over_time = false; 
  Client cl;
  int  a=0, client, counter_late, day = -1, final_day = 86399, late, total_late = 0, num_element = 1, order;
  OrderGroup og;
  pair<int, int> time_window;
  vector<int> timetable(0);
  bool print = true;


  os << "Violations on Time Windows";

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      over_time= false;
      day = -1;
      Route r = st.Routes(i);
      timetable = st.TimeTable(i);
      late = 0;
      counter_late = 0;
     
      for (unsigned k=0; k<r.Size(); k++)
	{
	  order = r[k]; 
	  if(r[k]!=-1)
	    {
	      og = st.OrderGroupVector(order);
	      num_element = og.Size();
	      client = st.IndexClient(og.IdClient());
	    }
	  else 
	    {
	      client = 0; 
	      num_element = 1;
	    }
	  cl = st.ClientVector(client);	 
	  time_window = cl.TimeWindow();
	  time_window.first = time_window.first*60;  //time window in min e timetable in sec
	  time_window.second = time_window.second*60;

	  if (k>=1)
	    {
	      if(timetable[k]<timetable[k-1])
		{
		  over_time = true;
		  day++;
		} 
	    }

	  if ( (timetable[k] > time_window.second) || over_time)
	    {
	      counter_late++;
	      if (over_time)
		{
		  a = final_day-time_window.second;
		  a = a + timetable[k] + day*86400; 
		  a = (int(double(a)/60 +0.5));
		  late+= a*num_element;
		}
	      else
		{
		  a= int(double(timetable[k]-time_window.second)/60 + 0.5);
		  late+=  a*num_element;
		  
		}

	      if(print)
		{
		  os << endl << "Route: " << setw(2) << i << ", " << "Late arrivals: ";
		  print = false;
		}

	      if(client != 0)
		{
		  for(unsigned  j=0; j<og.Size(); j++)
		    {
			os  << og[j] << "(" << a << " min) ";
		    }
		}
	      else
		 os  << "depot(" << a << " min)";

	    }
	}
      if (counter_late > 0)
	{
	  os << ", Total late: " << late/60 << " hours, " << late%60 << " minutes, Cost: " << TIMEWINDOW_COST*late;
	  total_late += late;
	}
      print = true;
    }

   os << endl << "Total late: " << total_late << " minutes" << endl;
   os  <<  "Total Cost: " << TIMEWINDOW_COST*(total_late) << endl << endl;
}

void VR_Validator :: PrintViolationsOnOutOrders(ostream& os) const
{
  int cost = 0, count = 0; 
  OrderGroup og;

  Route r = st.Routes(st.NumRoutes());

  os << "Violations on Unscheduled Orders" << endl;
  
  if (r.Size()>2)
    {

      os << "Unscheduled Orders:" << endl;
      for (unsigned i=1; i< r.Size()-1; i++) // route[0] and route[route.size()-1] are depot's fake orders
	{

	  og = in.OrderGroupVector(r[i]);

	  for(unsigned  j=0; j<og.Size(); j++)
	    {
	      os << setw(4) << og[j]
		 << ", Cost: " << setw(7) <<  OUTORDER_COST * og.Cost() << endl;
	      cost += og.Cost();
	      count++;
	    }
	}
    }

  os << "Total number of orders out the plannin horizon: " << count << endl;
  os  << "Total Cost Component : " << OUTORDER_COST * cost << endl << endl;  
}

void VR_Validator :: PrintVehicleFixedCosts(ostream& os) const
{
  Vehicle v;

  os << "Vehicle fixed costs" << endl;
  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      v = st.VehicleVector(r.Vehic());
      os << "Route: " << setw(2) << r.Index()
	 << ", Vehicle: " << setw(2) << r.Vehic() << ", used ";
      if (r.Size() > 2)
	{
	  os  << " Yes" << ", Fixed cost: " << setw(5) << VEHICLE_COST * v.FixedCost() << endl;
	} 
      else 	  
	os  << " No" << endl;
    }

  os << "Total Cost Component: " << FixedCostsOnVehicles() << endl << endl; 
}

void VR_Validator :: PrintVehicleVariableCosts(ostream& os) const
{
  Vehicle v;
  unsigned ind_courier;

  os << "Vehicle variable costs" << endl;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      v = st.VehicleVector(r.Vehic());
      os << "Route: " << setw(2) << r.Index()
	 << ", Vehicle: " << setw(2) << r.Vehic() << ", used ";
      if (r.Size() > 2)
	{
	  os  << " Yes, ";
	  ind_courier = in.IndexCourier(v.Courier());
	  in.CourierVector(ind_courier).GetBilling()->PrintCostFunction(r, i, os);  
	} 
      else 	  
	os  << " No" << endl;
    }

  os << "Total Cost Component: " << VariableCostsOnVehicles() << endl << endl; 
}

// void VR_Validator :: PrintVehicleCosts(ostream& os) const
// {
//   Vehicle v;
//   unsigned ind_courier;

//   os << "Vehicle costs" << endl;

//   for(unsigned i=0; i<st.NumRoutes(); i++)
//     {
//       Route r = st.Routes(i);
//       v = st.VehicleVector(r.Vehic());
//       os << "Route: " << setw(2) << r.Index()
// 	 << ", Vehicle: " << setw(2) << r.Vehic() << ", used ";
//       if (r.Size() > 2)
// 	{
// 	  os  << " Yes";
// 	  os  << ", Fixed vehicle cost: " << setw(5) << VEHICLE_COST * v.FixedCost() << ", ";
// 	  ind_courier = in.IndexCourier(v.Courier());
// 	  in.CourierVector(ind_courier).GetBilling()->PrintCostFunction(r, i, os);  
// 	} 
//       else 	  
// 	os  << " No" << endl;

//     }

//   os << "Total Cost Component: " << CostsOnVehicles() << endl; 
// }

void VR_Validator :: PrintDistanceCosts(ostream& os) const
{

  long int route_length = 0;

  os  << "Distance costs" << endl;

  for(unsigned i=0; i<st.NumRoutes(); i++)
    {
      Route r = st.Routes(i);
      os << "Route: " << setw(2) << i 
	 << ", distance: " << setw(4) << r.Length() << " km" 
	 << ", cost: "  << DISTANCE_COST*r.Length()*(int)st.VehicleVector(r.Vehic()).VarCost() << endl;
      route_length += r.Length();
    }

  os << "Total Distance: " << route_length << " km, " << "Total Cost Component " << DISTANCE_COST*route_length << endl; 

}
