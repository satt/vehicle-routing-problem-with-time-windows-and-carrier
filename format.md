## Input format 

The file starts with a header summarizing the main features of the instance, each on a single line, as in the following example:


    NAME: case1-A
    TYPE: HVRPFDTW 
    COMMENT: original
    NUM_CLIENTS: 44
    NUM_ORDERS: 139
    NUM_VEHICLES: 7
    NUM_CARRIERS: 4
    NUM_BILLINGS: 6
    NUM_REGIONS: 19
    EDGE_WEIGHT_TYPE: EXPLICIT
    EDGE_WEIGHT_FORMAT: FULL_MATRIX
    PLAN_HORIZON: 1 - 3;
 
 - **NAME:** the name of the instance 
 - **TYPE:** the type of the problem 
- **COMMENT:** a comment or a description of the instance 
- **NUM_CLIENTS:** the number of customers (i.e., the length of the subsequent CLIENTS section) 
- **NUM_ORDERS:** the number of orders (i.e., the length of the subsequent ORDERS section) 
- **NUM_VEHICLES:** the maximum number of vehicles available in each day (i.e., the length of the subsequent VEHICLES section) 
- **NUM_CARRIERS:** number of carriers (i.e., the length of the subsequent CARRIERS section) 
- **NUM_BILLINGS:** the number of different vehicle cost functions 
- **NUM_REGIONS:** the number of different regions where customers are located (i.e., the length of the subsequent REGIONS section) 
- **EDGE_WEIGHT_TYPE:** how edge weight is specified (EUC_2D, MAN_2D, MAX_2D, EXPLICIT) 
- **EDGE_WEIGHT_FORMAT:** for EDGE_WEIGHT_TYPE EXPLICIT, it is the specification of the shape of matrix (FULL_MATRIX, LOWER_TRIANG, ADJ, ...) 
- **PLAN_HORIZON:** the plannig period comprises all the days from *first_day* to *last_day* (*first_day* and *last_day* included)   

The DATA_SECTION separates specification from data section. It contains the following parts: 

-**DEPOT_SECTION:**  *depot_id*     
The identifier of the depot.

-**REGIONS:** *region_id1* *region_id2* ...     
It is a list of identifiers of regions.

-**CARRIERS:** *carrier_id* *billing_id* *num_incompatible_regions* *id_incompatible_region1* *id_incompatible_region2*    
Each line contains the information about a single carrier. The data is the name of the carrier (a valid identifier with no spaces), the name of the billing (a valid identifier with no spaces), the number of incompatible regions and the list of incompatible regions, separated by spaces as in the following example:

            cr2 b2 1 r23
    
This line indicates that carrier *cr2* uses the billing *b2* and it has 1 incompatible region which is *r23*.  

-**VEHICLES:** *vehicle_id* *capacity* *cost* *carrier_id*    
Each line contains the information about a single vehicle. The data is the name of the vehicle (a valid identifier with no spaces), its capacity (in Kg), its use-cost (in €) and the carrier identifier (a valid identifier with no spaces) which belongs to. The data is separated by spaces as in the following example:

        v1 50000 100 cr4

This line indicates that vehicle *v1*, that belongs to carrier *cr4*, has a maximum capacity of 50000 kilograms and a fixed cost of 100 euros. 

-**CLIENTS:** *client_id* *region_id* *ready_time* *due_date* *service_duration*    
Each line contains the information about a single client. The data is the name
of the client (a valid identifier with no spaces), the name of the region (a valid
identifier with no spaces) where the client is located, the time which service
must begin and end (in minutes), the duration of the visit (in minutes). The dat
is separated by spaces as in the following example:

            c43 r39 420 1020 60
    
This line indicates that for client *c43*, located in region *r39*, the visit must start after 420 (i.e. 7 am) and end before 1020 (i.e. 5 p.m.).  

-**ORDERS:** *order_id* *client_id* *quantity* *mandatory*(bool) *ready_date* *due_date*      
Each line contains the information about a single order. The data is the name of the order (a valid identifier with no spaces), the name of the client that made the order, its quantity (in Kg), if the order is mandatory, the ready date and the due date. The data is separated by spaces as in the following example:

    o0 c516  1562 0 1 3

This line indicates that order *o0*, made by client *c516*, has a quantity of 1512 kilograms, is optional and must be delivered from day 1 to day 3 of the planning horizon (day 1 and 3 included).  

-**EDGE_WEIGHT_SECTION:** *client1_id* *client2_id* *distance* *time_distance*    
Each line contains the distances between each couple of clients. The data is the name of the first client and second client (a valid identifier with no spaces) and the distance between this couple of clients expressed in Km and seconds. The data is separated by spaces as in the following example:

    c0 c43 213.688 7358
    
This line indicates that the distance between client *c0* and client *c43* is 213.688 kilometers or 7358 seconds. 

## Output format

A solution is presented as a list of routes for each day. The fleet available is the same in each day of the planning horizon. It is taken for granted that every route must start and end at depot. In addition there a the list of unscheduled orders which are not delivered.

-**Day day_number:** *route_number* *vehicle_id(vehicle_id.capacity)* *number_orders_route: order_id1 order_id2 ... route_demand*    
Each line contains the information about the route *route_number* in the day *day_number**. The data is the name of the vehicle that performs the route, its maximum capacity, the number of orders that are in the route and the list of these orders. Finally there is the total demand of all orders delivered in this route. This is an example:

    0  v1(5000) 5: o133 o75 o86 o87 o88 [3526]

This line indicates that route 0, performed by vehicle *v1* of maximum capacity 5000 kilograms, has 5 orders, which are *o133, o75, o86, o87* and *o88*. The sum of the demand of each order is 3526 kilograms.  

-**Unscheduled** *number_orders_unscheduled: order_id1 order_id2 ... route_demand*    
The line contains the information about unscheduled orders: the total number of unscheduled orders, their names and the total demand undelivered. In the following example:

    Unscheduled 3: o43 o22 o4 [1501]

There are 3 orders unscheduled (*o43 o22 o4*) with total demand of 1501 kilograms.
