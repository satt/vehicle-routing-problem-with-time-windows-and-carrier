# The Vehicle Routing Problem with Time Windows and Carrier-Dependent Costs

This repository contains instances, solutions and validator for the problem defined in *Tabu search techniques for the heterogeneous vehicle routing problem   with time windows and carrier-dependent costs* by Sara Ceschia, Luca Di Gaspero, and Andrea Schaerf, Journal of Scheduling, 14(6):601-615, 2011.

## Instances

Instances are stored in folder [`Instances`](Instances). 

## Solutions

Best solutions are available in the folder [`Solutions`](Solutions).

## File formats description

The file format of the instances and solutions are described in [`format.md`](format.md).

## Solution validator

The solution validator is available as the C++ source file [`validator.cc`](validator.cc). The compilation command is provided on top of the file as a comment. 

